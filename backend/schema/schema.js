const graphql = require("graphql");
const { login } = require("../functions/login");
const { registerStudent, registerCompany } = require("../functions/register");
// const { ApolloServer } = require("apollo-server");
const {
  getStudentProfile,
  updateStudentProfile,
  updateStudentBasicDetails,
  updateStudentEducation,
  updateStudentExperience,
  updateStudentJourney,
  getAllStudents,
  getCompanyJobs,
  getSelectedJob,
  applyJob,
  getStudentApplications
} = require("../functions/student");
const {
  getCompanyProfile,
  updateCompanyProfile,
  addCompanyJob,
  getJobStudents
} = require("../functions/company");

const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLSchema,
  GraphQLID,
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull
} = graphql;

const StudentType = new GraphQLObjectType({
  name: "Student",
  fields: () => ({
    status: { type: GraphQLString },
    id: { type: GraphQLString },
    name: { type: GraphQLString },
    school: { type: GraphQLString },
    DEGREE: { type: GraphQLString },
    MAJOR: { type: GraphQLString },
    GPA: { type: GraphQLString },
    dob: { type: GraphQLString },
    city: { type: GraphQLString },
    state: { type: GraphQLString },
    country: { type: GraphQLString },
    phone: { type: GraphQLString },
    email: { type: GraphQLString },
    Education: {
      type: new GraphQLList(EducationType),
      resolve(parent, args) {
        return parent.Education;
      }
    },
    Experience: {
      type: new GraphQLList(ExperienceType),
      resolve(parent, args) {
        return parent.Experience;
      }
    },
    Skill: {
      type: new GraphQLList(SkillType),
      resolve(parent, args) {
        return parent.Skill;
      }
    },
    PHOTO: { type: GraphQLString },
    journey: { type: GraphQLString }
  })
});

const EducationType = new GraphQLObjectType({
  name: "Education",
  fields: () => ({
    _id: { type: GraphQLString },
    COLLEGE_NAME: { type: GraphQLString },
    DEGREE: { type: GraphQLString },
    MAJOR: { type: GraphQLString },
    YEAR_OF_PASSING: { type: GraphQLString },
    CURRENT_GPA: { type: GraphQLString }
  })
});

const ExperienceType = new GraphQLObjectType({
  name: "Experience",
  fields: () => ({
    _id: { type: GraphQLString },
    COMPANY_NAME: { type: GraphQLString },
    TITLE: { type: GraphQLString },
    LOCATION: { type: GraphQLString },
    START_DT: { type: GraphQLString },
    END_DT: { type: GraphQLString },
    WORK_DESC: { type: GraphQLString }
  })
});

const SkillType = new GraphQLObjectType({
  name: "Skill",
  fields: () => ({
    _id: { type: GraphQLString },
    SKILL: { type: GraphQLString }
  })
});

const StudentAllType = new GraphQLObjectType({
  name: "StudentAll",
  fields: () => ({
    status: { type: GraphQLString },
    studentArr: {
      type: new GraphQLList(studentArrType),
      resolve(parent, args) {
        return parent.studentArr;
      }
    }
  })
});

const studentArrType = new GraphQLObjectType({
  name: "studentArr",
  fields: () => ({
    SID: { type: GraphQLString },
    name: { type: GraphQLString },
    profilePic: { type: GraphQLString },
    school: { type: GraphQLString },
    MAJOR: { type: GraphQLString },
    DEGREE: { type: GraphQLString },
    YEAR_OF_PASSING: { type: GraphQLString },
    SkillList: { type: GraphQLString }
  })
});

const GetJobType = new GraphQLObjectType({
  name: "GetJob",
  fields: () => ({
    status: { type: GraphQLString },
    jobArr: {
      type: new GraphQLList(JobObjType),
      resolve(parent, args) {
        return parent.jobArr;
      }
    },
    jobObj: {
      type: JobObjType
    }
  })
});

const JobObjType = new GraphQLObjectType({
  name: "JobObj",
  fields: () => ({
    _id: { type: GraphQLString },
    CID: { type: CompanyType },
    C_NAME: { type: GraphQLString },
    JOB_TITLE: { type: GraphQLString },
    APP_DEADLINE: { type: GraphQLString },
    CITY: { type: GraphQLString },
    STATE: { type: GraphQLString },
    COUNTRY: { type: GraphQLString },
    SALARY: { type: GraphQLString },
    JOB_DESC: { type: GraphQLString },
    JOB_CATEGORY: { type: GraphQLString },
    POST_FLAG: { type: GraphQLString },
    Created_At: { type: GraphQLString }
  })
});

const CompanyType = new GraphQLObjectType({
  name: "Company",
  fields: () => ({
    _id: { type: GraphQLString },
    NAME: { type: GraphQLString },
    EMAIL: { type: GraphQLString },
    PASSWORD: { type: GraphQLString },
    LOCATION: { type: GraphQLString },
    DESCRIPTION: { type: GraphQLString },
    PHONE: { type: GraphQLString },
    PROFILE_PIC: { type: GraphQLString }
  })
});

const StudentApplicationsType = new GraphQLObjectType({
  name: "StudentApplications",
  fields: () => ({
    jobListArr: {
      type: new GraphQLList(StudentApplicationsObjType),
      resolve(parent, args) {
        return parent.jobListArr;
      }
    }
  })
});

const StudentApplicationsObjType = new GraphQLObjectType({
  name: "StudentApplicationsObj",
  fields: () => ({
    _id: { type: GraphQLString },
    JID: { type: JobObj2Type },
    SID: { type: GraphQLString },
    STATUS: { type: GraphQLString },
    RESUME: { type: GraphQLString },
    CreatedAt: { type: GraphQLString }
  })
});

const JobObj2Type = new GraphQLObjectType({
  name: "JobObj2",
  fields: () => ({
    _id: { type: GraphQLString },
    CID: { type: GraphQLString },
    C_NAME: { type: GraphQLString },
    JOB_TITLE: { type: GraphQLString },
    APP_DEADLINE: { type: GraphQLString },
    CITY: { type: GraphQLString },
    STATE: { type: GraphQLString },
    COUNTRY: { type: GraphQLString },
    SALARY: { type: GraphQLString },
    JOB_DESC: { type: GraphQLString },
    JOB_CATEGORY: { type: GraphQLString },
    POST_FLAG: { type: GraphQLString },
    Created_At: { type: GraphQLString }
  })
});

const StudentJobType = new GraphQLObjectType({
  name: "StudentJob",
  fields: () => ({
    status: { type: GraphQLString },
    studentList: {
      type: new GraphQLList(StudentJobListType),
      resolve(parent, args) {
        return parent.studentList;
      }
    }
  })
});

const StudentJobListType = new GraphQLObjectType({
  name: "StudentJobList",
  fields: () => ({
    _id: { type: GraphQLString },
    JID: { type: GraphQLString },
    SID: { type: Student2Type },
    STATUS: { type: GraphQLString },
    RESUME: { type: GraphQLString },
    CreatedAt: { type: GraphQLString }
  })
});

const Student2Type = new GraphQLObjectType({
  name: "Student2",
  fields: () => ({
    _id: { type: GraphQLString },
    NAME: { type: GraphQLString },
    PHOTO: { type: GraphQLString },
    EDUCATION: {
      type: new GraphQLList(EducationType),
      resolve(parent, args) {
        return parent.EDUCATION;
      }
    },
    COLLEGE_NAME: { type: GraphQLString }
  })
});

const FileType = new GraphQLObjectType({
  name: "file",
  fields: () => ({
    filename: { type: GraphQLString },
    mimetype: { type: GraphQLString },
    email: { type: GraphQLString },
    encoding: { type: GraphQLString }
  })
});

const StatusType = new GraphQLObjectType({
  name: "Status",
  fields: () => ({
    status: { type: GraphQLString },
    message: { type: GraphQLString }
  })
});

const RootQuery = new GraphQLObjectType({
  name: "RootQueryType",
  fields: {
    student: {
      type: StudentType,
      args: { sid: { type: GraphQLString } },
      resolve(parent, args) {
        return getStudentProfile(args);
      }
    },
    getAllStudents: {
      type: StudentAllType,
      args: {
        student_filter: { type: GraphQLString },
        major_filter: { type: GraphQLString },
        skill_filter: { type: GraphQLString }
      },
      resolve(parent, args) {
        return getAllStudents(args);
      }
    },
    getCompanyJobs: {
      type: GetJobType,
      args: {
        category_filter: { type: GraphQLString },
        company_filter: { type: GraphQLString },
        job_filter: { type: GraphQLString },
        location_filter: { type: GraphQLString }
      },
      resolve(parent, args) {
        console.log("in reesolve");
        console.log(args);
        return getCompanyJobs(args);
      }
    },
    getSelectedJob: {
      type: JobObjType,
      args: { job_id: { type: GraphQLString } },
      resolve(parent, args) {
        return getSelectedJob(args);
      }
    },
    getStudentApplications: {
      type: StudentApplicationsType,
      args: {
        pending_filter: { type: GraphQLString },
        reviewed_filter: { type: GraphQLString },
        declined_filter: { type: GraphQLString },
        sid: { type: GraphQLString }
      },
      // async
      resolve(parent, args) {
        // let res = await
        return getStudentApplications(args);
        // console.log(res);
        // return res;
      }
    },
    getCompanyProfile: {
      type: CompanyType,
      args: {
        cid: { type: GraphQLString }
      },
      resolve(parent, args) {
        return getCompanyProfile(args);
      }
    },
    getJobStudents: {
      type: StudentJobType,
      args: {
        jid: { type: GraphQLString }
      },
      resolve(parent, args) {
        return getJobStudents(args);
      }
    }
  }
});

const Mutation = new GraphQLObjectType({
  name: "Mutation",
  fields: {
    login: {
      type: StatusType,
      args: {
        category: { type: GraphQLString },
        email_id: { type: GraphQLString },
        password: { type: GraphQLString }
      },
      resolve(parent, args) {
        return login(args);
      }
    },
    registerStudent: {
      type: StatusType,
      args: {
        name: { type: GraphQLString },
        email: { type: GraphQLString },
        password1: { type: GraphQLString },
        college_name: { type: GraphQLString }
      },
      async resolve(parent, args) {
        return registerStudent(args);
      }
    },
    registerCompany: {
      type: StatusType,
      args: {
        name: { type: GraphQLString },
        email: { type: GraphQLString },
        password1: { type: GraphQLString },
        location: { type: GraphQLString }
      },
      async resolve(parent, args) {
        return registerCompany(args);
      }
    },
    updateStudentProfile: {
      type: StatusType,
      args: {
        name: { type: GraphQLString },
        sid: { type: GraphQLString }
      },
      async resolve(parent, args) {
        return updateStudentProfile(args);
      }
    },
    updateStudentBasicDetails: {
      type: StatusType,
      args: {
        DOB: { type: GraphQLString },
        CITY: { type: GraphQLString },
        STATE: { type: GraphQLString },
        COUNTRY: { type: GraphQLString },
        PHONE: { type: GraphQLString },
        EMAIL: { type: GraphQLString },
        sid: { type: GraphQLString }
      },
      async resolve(parent, args) {
        return updateStudentBasicDetails(args);
      }
    },
    updateStudentJourney: {
      type: StatusType,
      args: {
        journey: { type: GraphQLString },
        sid: { type: GraphQLString }
      },
      async resolve(parent, args) {
        return updateStudentJourney(args);
      }
    },
    updateStudentEducation: {
      type: StatusType,
      args: {
        COLLEGE_NAME: { type: GraphQLString },
        DEGREE: { type: GraphQLString },
        MAJOR: { type: GraphQLString },
        YEAR_OF_PASSING: { type: GraphQLString },
        CURRENT_GPA: { type: GraphQLString },
        sid: { type: GraphQLString }
      },
      async resolve(parent, args) {
        return updateStudentEducation(args);
      }
    },
    updateStudentExperience: {
      type: StatusType,
      args: {
        COMPANY_NAME: { type: GraphQLString },
        TITLE: { type: GraphQLString },
        LOCATION: { type: GraphQLString },
        START_DT: { type: GraphQLString },
        END_DT: { type: GraphQLString },
        WORK_DESC: { type: GraphQLString },
        sid: { type: GraphQLString }
      },
      async resolve(parent, args) {
        return updateStudentExperience(args);
      }
    },
    applyJob: {
      type: StatusType,
      args: {
        JOB_CODE: { type: GraphQLString },
        SID: { type: GraphQLString }
      },
      async resolve(parent, args) {
        return applyJob(args);
      }
    },
    updateCompanyProfile: {
      type: StatusType,
      args: {
        cid: { type: GraphQLString },
        NAME: { type: GraphQLString },
        LOCATION: { type: GraphQLString },
        DESCRIPTION: { type: GraphQLString },
        PHONE: { type: GraphQLString }
      },
      resolve(parent, args) {
        return updateCompanyProfile(args);
      }
    },
    addCompanyJob: {
      type: StatusType,
      args: {
        cid: { type: GraphQLString },
        C_NAME: { type: GraphQLString },
        JOB_TITLE: { type: GraphQLString },
        APP_DEADLINE: { type: GraphQLString },
        CITY: { type: GraphQLString },
        STATE: { type: GraphQLString },
        COUNTRY: { type: GraphQLString },
        SALARY: { type: GraphQLString },
        JOB_DESC: { type: GraphQLString },
        JOB_CATEGORY: { type: GraphQLString }
      },
      resolve(parent, args) {
        return addCompanyJob(args);
      }
    }
  }
});

module.exports = new GraphQLSchema({
  query: RootQuery,
  mutation: Mutation
});
