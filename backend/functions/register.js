"use strict";
const bcrypt = require("bcrypt");
const { Student } = require("../model/studentModels");
const { Company } = require("../model/companyModels");
const passwordHash = require("password-hash");

//register Company
let registerCompany = async msg => {
  try {
    const company_register = await Company({
      NAME: msg.name,
      EMAIL: msg.email,
      PASSWORD: passwordHash.generate(msg.password1),
      LOCATION: msg.location
    });
    let savedRecord = await company_register.save();
    if (savedRecord) {
      console.log(JSON.stringify(savedRecord) + ": saved record");
      return { status: 200, message: "Company Record Inserted" };
    } else {
      return { status: 403, message: "Error caught" };
    }
  } catch (err) {
    console.log(err);
  }
};

//register Student
let registerStudent = async msg => {
  const student_register = await Student({
    NAME: msg.name,
    EMAIL: msg.email,
    PASSWORD: passwordHash.generate(msg.password1),
    COLLEGE_NAME: msg.college_name
  });
  let savedRecord = await student_register.save();
  if (savedRecord) {
    return { status: 200, message: "Student Record Inserted" };
  } else {
    return { status: 403, message: "Error caught while inserting record" };
  }
};

exports.registerStudent = registerStudent;
exports.registerCompany = registerCompany;
