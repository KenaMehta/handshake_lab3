"use strict";
const { Student } = require("../model/studentModels");
const {
  Company,
  Company_Jobs,
  Company_Events,
  Student_Job,
  Student_Event,
  Company_Profile
} = require("../model/companyModels");

//get company profile
let getCompanyProfile = async msg => {
  console.log(msg);
  const result = await Company.findOne({
    _id: msg.cid
  });
  if (result) {
    console.log(result);
    return result;
  }
};

//add company job
let addCompanyJob = async msg => {
  console.log(msg);
  try {
    var today = new Date();
    const addJob = await Company_Jobs({
      CID: msg.cid,
      C_NAME: msg.C_NAME,
      JOB_TITLE: msg.JOB_TITLE,
      APP_DEADLINE: msg.APP_DEADLINE,
      CITY: msg.CITY,
      STATE: msg.STATE,
      COUNTRY: msg.COUNTRY,
      SALARY: msg.SALARY,
      JOB_DESC: msg.JOB_DESC,
      JOB_CATEGORY: msg.JOB_CATEGORY,
      POST_FLAG: "Y",
      Created_At:
        today.getFullYear() +
        "-" +
        (today.getMonth() + 1) +
        "-" +
        today.getDate()
    });
    const result = await addJob.save();
    if (result) {
      return {
        status: 200,
        message: "Job Added Successfully"
      };
    }
  } catch (err) {
    console.log(err);
  }
};

//update company profile
let updateCompanyProfile = async msg => {
  console.log(msg);
  try {
    const result = await Company.findOne({ _id: msg.cid });
    if (result) {
      console.log(msg.NAME);
      let NAME = msg.NAME ? msg.NAME : result.NAME;
      console.log(NAME);
      let LOCATION = msg.LOCATION ? msg.LOCATION : result.LOCATION;
      let DESCRIPTION = msg.DESCRIPTION ? msg.DESCRIPTION : result.DESCRIPTION;
      let PHONE = msg.PHONE ? msg.PHONE : result.PHONE;

      let query = { _id: msg.cid };
      let update = {
        NAME: NAME,
        DESCRIPTION: DESCRIPTION,
        PHONE: PHONE,
        LOCATION: LOCATION
      };
      let options = { new: true, useFindAndModify: false };
      let company_basic_details = await Company.findOneAndUpdate(
        query,
        update,
        options
      );
      if (company_basic_details)
        return { status: 200, message: "Updated Company Profile" };
    }
  } catch (err) {
    console.log(err);
  }
};

//get students who applied for a particular job
let getJobStudents = async msg => {
  console.log(msg);
  try {
    const studentList = await Student_Job.find({
      JID: msg.jid
    }).populate("SID", "_id NAME PHOTO EDUCATION COLLEGE_NAME");
    return { status: 200, studentList };
  } catch (err) {
    console.log(err);
  }
};

//export company functions
exports.getCompanyProfile = getCompanyProfile;
exports.updateCompanyProfile = updateCompanyProfile;
exports.addCompanyJob = addCompanyJob;
exports.getJobStudents = getJobStudents;
