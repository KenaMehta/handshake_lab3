"use strict";
const { Student } = require("../model/studentModels");
const {
  Company,
  Company_Jobs,
  Student_Job
} = require("../model/companyModels");

//get student profile
let getStudentProfile = async msg => {
  try {
    console.log("in student profile");
    const result = await Student.findOne({ _id: msg.sid });
    if (result) {
      console.log(result);
      var COLLEGE_NAME = result.COLLEGE_NAME;
      var EducationArr = result.EDUCATION;
      var ExperienceArr = result.EXPERIENCE;
      var SkillArr = result.SKILL;
      var Edu = EducationArr.filter(ed => ed.COLLEGE_NAME == COLLEGE_NAME);
      if (Edu.length != 0) {
        console.log(Edu);
        var MAJOR = Edu[0].MAJOR;

        return {
          status: 200,
          id: msg.sid,
          name: result.NAME,
          school: result.COLLEGE_NAME,
          DEGREE: Edu[0].DEGREE,
          MAJOR: Edu[0].MAJOR,
          GPA: Edu[0].CURRENT_GPA,
          dob: result.DOB || "",
          city: result.CITY || "",
          state: result.STATE || "",
          country: result.COUNTRY || "",
          phone: result.PHONE || "",
          email: result.EMAIL,
          Education: EducationArr,
          Experience: ExperienceArr,
          Skill: SkillArr,
          PHOTO: result.PHOTO ? result.PHOTO : "",
          journey: result.CAREER_OBJ || ""
        };
      } else {
        return {
          status: 200,
          id: msg.sid,
          name: result.NAME,
          school: result.COLLEGE_NAME,
          DEGREE: "",
          MAJOR: "",
          GPA: "",
          dob: result.DOB || "",
          city: result.CITY || "",
          state: result.STATE || "",
          country: result.COUNTRY || "",
          phone: result.PHONE || "",
          email: result.EMAIL,
          Education: EducationArr,
          Experience: ExperienceArr,
          Skill: SkillArr,
          PHOTO: result.PHOTO ? result.PHOTO : "null",
          journey: result.CAREER_OBJ || ""
        };
      }
    } else {
      return {
        status: 403,
        res: "Unauthenticated Student"
      };
    }
  } catch (err) {
    console.log("Error caught when hitting Student Connection: " + err);
    return {
      status: 500,
      res: "Unable to fetch profile from Student"
    };
  }
};

//update student basic details
let updateStudentBasicDetails = async msg => {
  console.log(msg);
  let result = await Student.findOne({
    _id: msg.sid
  });
  if (result) {
    console.log(result + "res found");
    let DOB = msg.DOB ? msg.DOB : result.DOB;
    let CITY = msg.CITY ? msg.CITY : result.CITY;
    let STATE = msg.STATE ? msg.STATE : result.STATE;
    let COUNTRY = msg.COUNTRY ? msg.COUNTRY : result.COUNTRY;
    let PHONE = msg.PHONE ? msg.PHONE : result.PHONE;

    try {
      console.log(DOB + " major found");
      console.log(CITY + " yop found");
      console.log(STATE + " gpa found");

      let query = {
        _id: msg.sid
      };
      let update = {
        DOB: DOB,
        CITY: CITY,
        STATE: STATE,
        COUNTRY: COUNTRY,
        PHONE: PHONE
      };
      let options = { new: true, useFindAndModify: false };
      let basicDetailsUpdate = await Student.findOneAndUpdate(
        query,
        update,
        options
      );
      if (basicDetailsUpdate)
        return {
          status: 200,
          message: "Updated"
        };
      else return { status: 500, message: "Failed to Update" };
    } catch (err) {
      return { status: 500, message: err };
    }
  }
};

//Update Student Education
let updateStudentEducation = async msg => {
  console.log(msg);
  const result = await Student.findOne({
    _id: msg.sid,
    "EDUCATION.COLLEGE_NAME": msg.COLLEGE_NAME
  });
  if (result) {
    console.log(result);
    const Stu = await Student.findOne({ _id: msg.sid });
    var EDUCATION = Stu.EDUCATION;
    console.log("EDUCATION: " + EDUCATION[0].COLLEGE_NAME);
    var EDUCATION_other = EDUCATION.filter(edu => {
      console.log(edu.COLLEGE_NAME);
      return edu.COLLEGE_NAME != msg.COLLEGE_NAME;
    });
    var EDUCATION_update = EDUCATION.filter(edu => {
      console.log(edu.COLLEGE_NAME);
      return edu.COLLEGE_NAME == msg.COLLEGE_NAME;
    });
    let degree = msg.DEGREE ? msg.DEGREE : EDUCATION_update[0].DEGREE;
    let major = msg.MAJOR ? msg.MAJOR : EDUCATION_update[0].MAJOR;
    let yop = msg.YEAR_OF_PASSING
      ? msg.YEAR_OF_PASSING
      : EDUCATION_update[0].YEAR_OF_PASSING;
    let gpa = msg.CURRENT_GPA
      ? msg.CURRENT_GPA
      : EDUCATION_update[0].CURRENT_GPA;
    try {
      console.log(degree + " major found");
      console.log(yop + " yop found");
      console.log(gpa + " gpa found");
      EDUCATION_other.push({
        COLLEGE_NAME: msg.COLLEGE_NAME,
        DEGREE: degree,
        MAJOR: major,
        YEAR_OF_PASSING: yop,
        CURRENT_GPA: gpa
      });
      console.log(EDUCATION_other);
      let query = {
        _id: msg.sid
      };
      let update = {
        EDUCATION: EDUCATION_other
      };
      let options = { new: true, useFindAndModify: false };
      let educationAdd = await Student.findOneAndUpdate(query, update, options);
      if (educationAdd)
        return {
          status: 200,
          message: "Education updated"
        };
      else return { status: 403, message: "Education update failed" };
    } catch (err) {
      console.log(err);
      return { status: 500, message: err };
    }
  } else {
    console.log("in else");
    const Stu = await Student.findOne({ _id: msg.sid });
    const EDUCATION = Stu.EDUCATION;
    EDUCATION.push({
      COLLEGE_NAME: msg.COLLEGE_NAME,
      DEGREE: msg.DEGREE,
      MAJOR: msg.MAJOR,
      YEAR_OF_PASSING: msg.YEAR_OF_PASSING,
      CURRENT_GPA: msg.CURRENT_GPA
    });
    console.log(EDUCATION);
    const updated = await Stu.save();

    if (updated) return { status: 200, message: "New education added" };
    else return { status: 403, message: "New education add failed" };
  }
};

//update Student Experience
let updateStudentExperience = async msg => {
  console.log(msg);
  const result = await Student.findOne({
    _id: msg.sid,
    "EXPERIENCE.COMPANY_NAME": msg.COMPANY_NAME
  });

  if (result) {
    const Stu = await Student.findOne({ _id: msg.sid });
    var EXPERIENCE = Stu.EXPERIENCE;
    console.log("EXPERIENCE: " + EXPERIENCE[0].COMPANY_NAME);
    var EXPERIENCE_other = EXPERIENCE.filter(ex => {
      console.log(ex.COMPANY_NAME);
      return ex.COMPANY_NAME != msg.COMPANY_NAME;
    });
    var EXPERIENCE_update = EXPERIENCE.filter(ex => {
      console.log(ex.COMPANY_NAME);
      return ex.COMPANY_NAME == msg.COMPANY_NAME;
    });
    let title = msg.TITLE ? msg.TITLE : EXPERIENCE_update[0].TITLE;
    let location = msg.LOCATION ? msg.LOCATION : EXPERIENCE_update[0].LOCATION;
    let start_date = msg.START_DT
      ? msg.START_DT
      : EXPERIENCE_update[0].START_DT;
    let end_date = msg.END_DT ? msg.END_DT : EXPERIENCE_update[0].END_DT;
    let work_desc = msg.WORK_DESC
      ? msg.WORK_DESC
      : EXPERIENCE_update[0].WORK_DESC;
    try {
      console.log(title + " major found");
      console.log(location + " yop found");
      console.log(start_date + " gpa found");
      EXPERIENCE_other.push({
        COMPANY_NAME: msg.COMPANY_NAME,
        TITLE: title,
        LOCATION: location,
        START_DT: start_date,
        END_DT: end_date,
        WORK_DESC: work_desc
      });
      console.log(EXPERIENCE_other);
      let query = {
        _id: msg.sid
      };
      let update = {
        EXPERIENCE: EXPERIENCE_other
      };
      let options = { new: true, useFindAndModify: false };
      let experienceAdd = await Student.findOneAndUpdate(
        query,
        update,
        options
      );
      if (experienceAdd) return { status: 200, message: "Experience updated" };
      else return { status: 403, message: "Update experience failed" };
    } catch (err) {
      console.log(err);
      return { status: 500, message: "Error in experience" };
    }
  } else {
    console.log("in else");
    const Stu = await Student.findOne({ _id: msg.sid });
    const EXPERIENCE = Stu.EXPERIENCE;

    EXPERIENCE.push({
      COMPANY_NAME: msg.COMPANY_NAME,
      TITLE: msg.TITLE,
      LOCATION: msg.LOCATION,
      START_DT: msg.START_DT,
      END_DT: msg.END_DT,
      WORK_DESC: msg.WORK_DESC
    });
    console.log(EXPERIENCE);
    const updated = await Stu.save();
    if (updated) return { status: 200, message: "New experience added" };
    else return { status: 403, message: "New experience add failed" };
  }
};

//update student journey
let updateStudentJourney = async msg => {
  try {
    console.log(msg);
    let query = { _id: msg.sid };
    let update = { CAREER_OBJ: msg.journey };
    let options = { new: true, useFindAndModify: false };
    let result = await Student.findOneAndUpdate(query, update, options);
    if (result) {
      return { status: 200, message: "Journey Updated" };
    } else {
      return {
        status: 500,
        message: "Unable to fetch data from Student_Profile"
      };
    }
  } catch (err) {
    console.log(err);
    return { status: 500, message: err };
  }
};

//update Student Profile
let updateStudentProfile = async msg => {
  try {
    let query = { _id: msg.sid };
    let update = { NAME: msg.name };
    let options = { new: true, useFindAndModify: false };
    let student_name = await Student.findOneAndUpdate(query, update, options);
    if (student_name) return { status: 200, message: "Updated name" };
  } catch (err) {
    console.log(err);
    return { status: 500, message: err };
  }
};

//get all students
let getAllStudents = async msg => {
  console.log(msg);
  if (
    msg.student_filter == "none" &&
    msg.major_filter == "none" &&
    msg.skill_filter == "none"
  ) {
    const studentPaginateObject = await Student.find();
    var newStudentArr = [];
    studentPaginateObject.map(student => {
      var EducationArr = student.EDUCATION;
      var Edu = EducationArr.filter(
        ed => ed.COLLEGE_NAME == student.COLLEGE_NAME
      );
      var SkillArr = student.SKILL;
      var skillList = "";
      SkillArr.map(skill => {
        skillList
          ? (skillList = skillList + "," + skill.SKILL)
          : (skillList = skill.SKILL);
      });
      newStudentArr.push({
        SID: student._id,
        name: student.NAME,
        profilePic: student.PHOTO,
        school: student.COLLEGE_NAME,
        MAJOR: Edu.length != 0 ? Edu[0].MAJOR : "",
        DEGREE: Edu.length != 0 ? Edu[0].DEGREE : "",
        YEAR_OF_PASSING: Edu.length != 0 ? Edu[0].YEAR_OF_PASSING : "",
        SkillList: skillList
      });
    });
    return {
      status: 200,
      studentArr: newStudentArr
    };
  } else {
    const studentPaginateObject = await Student.find({
      $or: [
        {
          NAME: {
            $regex: new RegExp(msg.student_filter, "i")
          }
        },
        {
          COLLEGE_NAME: {
            $regex: new RegExp(msg.student_filter, "i")
          }
        },
        {
          "EDUCATION.MAJOR": {
            $regex: new RegExp(msg.major_filter, "i")
          }
        },
        {
          "SKILL.SKILL": {
            $regex: new RegExp(msg.skill_filter, "i")
          }
        }
      ]
    });
    var newStudentArr = [];
    studentPaginateObject.map(student => {
      var EducationArr = student.EDUCATION;
      var Edu = EducationArr.filter(
        ed => ed.COLLEGE_NAME == student.COLLEGE_NAME
      );
      var SkillArr = student.SKILL;
      var skillList = "";
      SkillArr.map(skill => {
        skillList
          ? (skillList = skillList + "," + skill.SKILL)
          : (skillList = skill.SKILL);
      });
      newStudentArr.push({
        SID: student._id,
        name: student.NAME,
        profilePic: student.PHOTO,
        school: student.COLLEGE_NAME,
        MAJOR: Edu.length != 0 ? Edu[0].MAJOR : "",
        DEGREE: Edu.length != 0 ? Edu[0].DEGREE : "",
        YEAR_OF_PASSING: Edu.length != 0 ? Edu[0].YEAR_OF_PASSING : "",
        SkillList: skillList
      });
    });
    return {
      status: 200,
      studentArr: newStudentArr
    };
  }
};

//get company jobs
let getCompanyJobs = async msg => {
  console.log(msg);
  var whereCondition = { POST_FLAG: "Y" };
  if (msg.category_filter !== "none") {
    whereCondition = {
      ...whereCondition,
      JOB_CATEGORY: msg.category_filter
    };
  }
  if (msg.company_filter !== "none") {
    console.log("inside if clause for company_fiiilter");
    whereCondition = {
      ...whereCondition,
      C_NAME: { $regex: new RegExp(msg.company_filter, "i") }
    };
  }
  if (msg.job_filter !== "none") {
    whereCondition = {
      ...whereCondition,
      JOB_TITLE: { $regex: new RegExp(msg.job_filter, "i") }
    };
  }
  if (msg.location_filter !== "none") {
    whereCondition = {
      ...whereCondition,
      CITY: { $regex: new RegExp(msg.location_filter, "i") }
    };
  }
  console.log(whereCondition);
  try {
    const fullData = await Company_Jobs.find(whereCondition).populate("CID");
    return {
      status: 200,
      jobArr: fullData,
      jobObj: fullData[0]
    };
  } catch (err) {
    console.log(err);
  }
};

//get selected job
let getSelectedJob = async msg => {
  console.log(msg);
  try {
    const selectedCompany = await Company_Jobs.findOne({
      _id: msg.job_id
    }).populate("CID");
    console.log(selectedCompany);
    return selectedCompany;
  } catch (err) {
    console.log(err);
  }
};

//apply for job
let applyJob = async msg => {
  try {
    console.log(msg);
    var today = new Date();
    const addedStudent = await Student_Job({
      JID: msg.JOB_CODE,
      SID: msg.SID,
      STATUS: "Pending",
      RESUME: "",
      CreatedAt:
        today.getFullYear() +
        "-" +
        (today.getMonth() + 1) +
        "-" +
        today.getDate()
    });
    const result = await addedStudent.save();
    if (result)
      return {
        status: 200,
        message: "  Applied successfully"
      };
    else {
      console.log(err);
      return {
        status: 403,
        message: "  Already applied"
      };
    }
  } catch (err) {
    console.log(err);
    return { status: 403, message: "  Already applied" };
  }
};

//get Student Application
let getStudentApplications = async msg => {
  console.log(msg);
  try {
    var whereOrCondition = {};
    if (msg.pending_filter == "true") {
      console.log("in pending");
      whereOrCondition = { STATUS: "Pending" };
      console.log("pending where " + JSON.stringify(whereOrCondition));
    }
    if (msg.reviewed_filter == "true") {
      console.log("in reviewed");
      whereOrCondition = { STATUS: "Reviewed" };
      console.log("reviewed where " + JSON.stringify(whereOrCondition));
    }
    if (msg.declined_filter == "true") {
      console.log("in declined");
      whereOrCondition = { STATUS: "Declined" };
      console.log("declined where " + JSON.stringify(whereOrCondition));
    }
    console.log(Object.keys(whereOrCondition).length);

    if (Object.keys(whereOrCondition).length === 0) {
      console.log("in if");
      const jobListArr = await Student_Job.find({
        SID: msg.sid
      }).populate("JID");
      console.log(jobListArr);
      return { jobListArr };
    } else {
      console.log("in else");
      const jobListArr = await Student_Job.find({
        SID: msg.sid,
        ...whereOrCondition
      }).populate("JID");
      return { jobListArr };
    }
  } catch (err) {
    console.log(err);
  }
};

//export student functions
exports.getStudentProfile = getStudentProfile;
exports.updateStudentProfile = updateStudentProfile;
exports.updateStudentBasicDetails = updateStudentBasicDetails;
exports.updateStudentEducation = updateStudentEducation;
exports.updateStudentExperience = updateStudentExperience;
exports.updateStudentJourney = updateStudentJourney;
exports.getAllStudents = getAllStudents;
exports.getCompanyJobs = getCompanyJobs;
exports.getSelectedJob = getSelectedJob;
exports.applyJob = applyJob;
exports.getStudentApplications = getStudentApplications;
