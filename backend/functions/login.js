"use strict";
const bcrypt = require("bcrypt");
const { Student } = require("../model/studentModels");
const { Company } = require("../model/companyModels");
const passwordHash = require("password-hash");

let login = async msg => {
  let model = "";
  console.log(msg);
  model = msg.category === "student" ? Student : Company;
  let email = msg.email_id;
  let user = await model.findOne({ EMAIL: email });
  if (user) {
    if (passwordHash.verify(msg.password, user.PASSWORD)) {
      return { status: 200, message: user._id };
    } else {
      return { status: 403, message: "Password Incorrect" };
    }
  } else {
    return { status: 403, message: "User not found" };
  }
};

exports.login = login;
