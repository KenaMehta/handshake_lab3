var chai = require("chai"),
  chaiHttp = require("chai-http");
chai.use(chaiHttp);
const api_host = "http://localhost";
const api_port = "3001";
const api_url = api_host + ":" + api_port;

var expect = chai.expect;

it("Check if API server is up and running", function(done) {
  chai
    .request(api_url)
    .get("/api")
    .send()
    .end(function(err, res) {
      expect(res).to.have.status(200);
      expect(res.text).to.equal("matched");
      done();
    });
});

it("Check if login is valid", function(done) {
  chai
    .request(api_url)
    .post("/login")
    .send({
      email: "kenamehta1995@gmail.com",
      password: "!123Qqwe",
      category: "student"
    })
    .end(function(err, res) {
      expect(res).to.have.status(200);
      expect(res.body.passed).to.equal(true);
      done();
    });
});

it("Validate profile details", function(done) {
  chai
    .request(api_url)
    .get("/student/profile/65f34d38-431a-4936-9ffa-d432c89d6f2c")
    .send()
    .end(function(err, res) {
      expect(res).to.have.status(200);
      expect(res.body).to.be.a("Object");
      expect(res.body.id).to.equal("65f34d38-431a-4936-9ffa-d432c89d6f2c");
      done();
    });
});

it("Checking if job search returns atleast one job", function(done) {
  chai
    .request(api_url)
    .get("/student/job_list/none/none/none/none")
    .send()
    .end(function(err, res) {
      expect(res).to.have.status(200);
      expect(res.body).to.be.a("Array");
      expect(res.body).to.have.length.greaterThan(1);
      done();
    });
});

it("Checking if event search returns atleast one event", function(done) {
  chai
    .request(api_url)
    .get("/student/event_list/none")
    .send()
    .end(function(err, res) {
      expect(res).to.have.status(200);
      expect(res.body).to.be.a("Array");
      expect(res.body).to.have.length.greaterThan(1);
      done();
    });
});


