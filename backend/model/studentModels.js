const Mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const uniqueValidator = require("mongoose-unique-validator");
// const mongoosePaginate = require("mongoose-paginate");
// const passportLocalMongoose = require("passport-local-mongoose");
// const passport = require("passport");

//student_education schema
const Student_Education = new Mongoose.Schema({
  COLLEGE_NAME: String,
  DEGREE: String,
  MAJOR: String,
  YEAR_OF_PASSING: String,
  CURRENT_GPA: String
});

//student_experience schema
const Student_Experience = new Mongoose.Schema({
  COMPANY_NAME: String,
  TITLE: String,
  LOCATION: String,
  START_DT: String,
  END_DT: String,
  WORK_DESC: String
});

//skills schema
const Student_Skill = new Mongoose.Schema({
  SKILL: String
});

//student schema
const StudentSchema = new Mongoose.Schema({
  NAME: String,
  EMAIL: { type: String, unique: true },
  PASSWORD: String,
  COLLEGE_NAME: String,
  DOB: String,
  CITY: String,
  STATE: String,
  COUNTRY: String,
  PHONE: String,
  CAREER_OBJ: String,
  PHOTO: String,
  EDUCATION: [Student_Education],
  EXPERIENCE: [Student_Experience],
  SKILL: [Student_Skill]
});
// StudentSchema.plugin(passportLocalMongoose);
// StudentSchema.plugin(mongoosePaginate);
StudentSchema.plugin(uniqueValidator);

const Student = Mongoose.model("Student", StudentSchema);

//passport
// passport.use(Student.createStrategy());
// passport.serializeUser(Student.serializeUser());
// passport.deserializeUser(Student.deserializeUser());

module.exports = {
  Student
};
