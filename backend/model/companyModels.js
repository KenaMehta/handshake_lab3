const Mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const uniqueValidator = require("mongoose-unique-validator");
const { Student } = require("./studentModels");
// const mongoosePaginate = require("mongoose-paginate");

//company profile schema
const Company_Profile_Schema = new Mongoose.Schema({
  NAME: String,
  EMAIL: { type: String, unique: true },
  PASSWORD: String,
  LOCATION: String,
  DESCRIPTION: String,
  PHONE: String,
  PROFILE_PIC: String
});
Company_Profile_Schema.plugin(uniqueValidator);
const Company = Mongoose.model("Company", Company_Profile_Schema);

//company_jobs schema
const Company_Jobs_Schema = new Mongoose.Schema({
  CID: { type: Mongoose.Schema.Types.ObjectId, ref: "Company" },
  C_NAME: String,
  JOB_TITLE: String,
  APP_DEADLINE: String,
  CITY: String,
  STATE: String,
  COUNTRY: String,
  SALARY: String,
  JOB_DESC: String,
  JOB_CATEGORY: String,
  POST_FLAG: String,
  Created_At: String
});
// Company_Jobs_Schema.plugin(mongoosePaginate);
const Company_Jobs = Mongoose.model("Company_Jobs", Company_Jobs_Schema);

//Student_Job Schema
const Student_Job_Schema = new Mongoose.Schema({
  SID: { type: Mongoose.Schema.Types.ObjectId, ref: "Student" },
  JID: { type: Mongoose.Schema.Types.ObjectId, ref: "Company_Jobs" },
  CID: String,
  STATUS: String,
  RESUME: String,
  CreatedAt: String
});
Student_Job_Schema.index({ SID: 1, JID: 1 }, { unique: true });
// Student_Job_Schema.plugin(mongoosePaginate);
// Student_Job_Schema.plugin(uniqueValidator);
const Student_Job = Mongoose.model("Student_Job", Student_Job_Schema);

//company_events schema
const Company_Events_Schema = new Mongoose.Schema({
  CID: { type: Mongoose.Schema.Types.ObjectId, ref: "Company" },
  E_NAME: String,
  E_DESC: String,
  TIME: String,
  DATE: Date,
  LOCATION: String,
  ELIGIBILITY: String,
  POST_FLAG: String,
  Created_At: Date
});
// Company_Events_Schema.plugin(mongoosePaginate);
const Company_Events = Mongoose.model("Company_Events", Company_Events_Schema);

//Student_Event Schema
const Student_Event_Schema = new Mongoose.Schema({
  SID: { type: Mongoose.Schema.Types.ObjectId, ref: "Student" },
  EID: {
    type: Mongoose.Schema.Types.ObjectId,
    ref: "Company_Events"
  },
  CID: String
});
Student_Event_Schema.index({ SID: 1, EID: 1 }, { unique: true });
Student_Event_Schema.plugin(uniqueValidator);
const Student_Event = Mongoose.model("Student_Event", Student_Event_Schema);

//exporting Company's models
module.exports = {
  Company,
  Company_Jobs,
  Company_Events,
  Student_Job,
  Student_Event
};
