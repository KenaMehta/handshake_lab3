import { gql } from "apollo-boost";

export const getStudentQuery = gql`
  query($sid: String) {
    student(sid: $sid) {
      status
      id
      name
      school
      DEGREE
      MAJOR
      GPA
      dob
      city
      state
      country
      phone
      email
      Education {
        _id
        COLLEGE_NAME
        DEGREE
        MAJOR
        YEAR_OF_PASSING
        CURRENT_GPA
      }
      Experience {
        _id
        COMPANY_NAME
        TITLE
        LOCATION
        START_DT
        END_DT
        WORK_DESC
      }
      Skill {
        _id
        SKILL
      }
      PHOTO
      journey
    }
  }
`;

export const getAllStudentsQuery = gql`
  query($student_filter: String, $major_filter: String, $skill_filter: String) {
    getAllStudents(
      student_filter: $student_filter
      major_filter: $major_filter
      skill_filter: $skill_filter
    ) {
      status
      studentArr {
        SID
        name
        profilePic
        school
        MAJOR
        DEGREE
        YEAR_OF_PASSING
        SkillList
      }
    }
  }
`;

export const getCompanyJobsQuery = gql`
  query(
    $category_filter: String
    $company_filter: String
    $job_filter: String
    $location_filter: String
  ) {
    getCompanyJobs(
      category_filter: $category_filter
      company_filter: $company_filter
      job_filter: $job_filter
      location_filter: $location_filter
    ) {
      status
      jobArr {
        _id
        CID {
          _id
          PROFILE_PIC
        }
        C_NAME
        JOB_TITLE
        APP_DEADLINE
        CITY
        STATE
        COUNTRY
        SALARY
        JOB_DESC
        JOB_CATEGORY
        POST_FLAG
        Created_At
      }
      jobObj {
        _id
        CID {
          _id
          PROFILE_PIC
        }
        C_NAME
        JOB_TITLE
        APP_DEADLINE
        CITY
        STATE
        COUNTRY
        SALARY
        JOB_DESC
        JOB_CATEGORY
        POST_FLAG
        Created_At
      }
    }
  }
`;

export const getSelectedJobQuery = gql`
  query($job_id: String) {
    getSelectedJob(job_id: $job_id) {
      _id
      CID {
        _id
        PROFILE_PIC
      }
      C_NAME
      JOB_TITLE
      APP_DEADLINE
      CITY
      STATE
      COUNTRY
      SALARY
      JOB_DESC
      JOB_CATEGORY
      POST_FLAG
      Created_At
    }
  }
`;

export const getStudentApplicationsQuery = gql`
  query(
    $pending_filter: String
    $reviewed_filter: String
    $declined_filter: String
    $sid: String
  ) {
    getStudentApplications(
      pending_filter: $pending_filter
      reviewed_filter: $reviewed_filter
      declined_filter: $declined_filter
      sid: $sid
    ) {
      jobListArr {
        SID
        JID {
          _id
          CID
          C_NAME
          JOB_TITLE
          APP_DEADLINE
          CITY
          STATE
          COUNTRY
          SALARY
          JOB_DESC
          JOB_CATEGORY
          POST_FLAG
          Created_At
        }
        STATUS
        RESUME
        CreatedAt
      }
    }
  }
`;

export const getCompanyProfileQuery = gql`
  query($cid: String) {
    getCompanyProfile(cid: $cid) {
      _id
      NAME
      EMAIL
      PASSWORD
      LOCATION
      DESCRIPTION
      PHONE
      PROFILE_PIC
    }
  }
`;

export const getJobStudentsQuery = gql`
  query($jid: String) {
    getJobStudents(jid: $jid) {
      status
      studentList {
        _id
        JID
        SID {
          _id
          NAME
          PHOTO
          EDUCATION {
            _id
            COLLEGE_NAME
            DEGREE
            MAJOR
            YEAR_OF_PASSING
            CURRENT_GPA
          }
          COLLEGE_NAME
        }
        STATUS
        RESUME
        CreatedAt
      }
    }
  }
`;
