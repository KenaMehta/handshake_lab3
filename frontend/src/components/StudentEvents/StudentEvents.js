import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
import "../styles/events.css";
import { connect } from "react-redux";
import cookie from "react-cookies";
import { Redirect } from "react-router";
import axios from "axios";
import configPath from "./../../configApp";

class StudentEvents extends Component {
  constructor(props) {
    super(props);
    this.state = {
      event_filter: "none",
      eventArr: [],
      eventObj: {},
      eventRegArr: [],
      modalBox: "HideBox",
      application_status: "Register",
      showRegButton: true
    };
  }

  modalUpdate = () => {
    this.setState({ application_status: "" });
    if (this.state.modalBox == "DisplayBox")
      this.setState({ modalBox: "HideBox" });
    else this.setState({ modalBox: "DisplayBox" });
  };

  displayEvent = event_id => {
    console.log("in getPage");
    axios
      .post(configPath.api_host + `/student/event_list`, { event_id: event_id })
      .then(response => {
        if (response.status === 200) {
          this.setState({ eventObj: response.data });
        }
      })
      .catch(err => console.log(err + " : Error in getting Profile details"));
  };

  getPage = () => {
    console.log(this.state.event_filter);
    axios
      .get(
        configPath.api_host + `/student/event_list/${this.state.event_filter}`
      )
      .then(response => {
        if (response.status === 200) {
          this.setState({ eventArr: response.data });
          this.setState({ eventObj: response.data[0] });
        }
      })
      .catch(err => console.log(err + " : Error in getting event details"));
  };
  callGetPage = () => {
    console.log(this.state.event_filter + "filter");
    this.getPage();
  };

  studentApply = e => {
    e.preventDefault();
    console.log("In studentApply");
    axios
      .post(configPath.api_host + `/student/event_list/event_apply`, {
        SID: localStorage.getItem("SID"),
        E_CODE: this.state.eventObj.E_CODE,
        ELIGIBILITY: this.state.eventObj.ELIGIBILITY
      })
      .then(response => {
        console.log(JSON.stringify(response.data) + "iiin stu app");
        this.setState({ application_status: response.data.application_status });
        this.getRegEvents();
      })
      .catch(error => {
        console.log(error);
        this.setState({
          application_status: error.response.data.application_status
        });
      });
  };
  getRegEvents = () => {
    console.log("In getRegEvents");
    axios
      .post(configPath.api_host + `/student/event_list/regEvents`, {
        SID: localStorage.getItem("SID")
      })
      .then(response => {
        console.log(JSON.stringify(response.data) + "reg events");
        this.setState({ eventRegArr: response.data });
      })
      .catch(error => {
        console.log(error);
      });
  };

  componentDidMount() {
    console.log("In componentDidMount");
    this.getPage();
    this.getRegEvents();
  }

  render() {
    // if (!cookie.load("cookie")) {
    //   return <Redirect to="/login" />;
    // }
    if (localStorage.getItem("loginFlag") != "true") {
      return <Redirect to="/login" />;
    }

    return (
      <div>
        <Row align="center">
          <Col className="head_line" md={12}>
            <h2 style={{ opacity: 0.7 }}>Events</h2>
          </Col>
        </Row>
        <Container>
          <Row>
            <Col md={3}>
              <Card
                bg="light"
                className="mt-3 d-flex"
                style={{ width: "auto", marginBottom: "20px", height: "500px" }}
              >
                <div className="style__jobs___3seWY p-2">
                  <h4 className="card-title p-2" align="center">
                    List of Events
                  </h4>
                  <div
                    className="d-flex mb-3"
                    style={{ left: "-10px", position: "relative" }}
                  >
                    <div
                      className="m-2"
                      style={{ left: "30px", top: "7px", position: "relative" }}
                    >
                      <ion-icon name="search-outline" />
                    </div>
                    <input
                      style={{ width: "auto" }}
                      className="mt-2 pl-4"
                      onChange={e => {
                        this.setState({ event_filter: e.target.value }, () => {
                          if (this.state.event_filter) {
                            this.callGetPage();
                          } else {
                            this.setState({ event_filter: "none" }, () => {
                              this.callGetPage();
                            });
                          }
                        });
                      }}
                      id="display_name"
                      //style={{ margin: "20px" }}
                      type="text"
                      placeholder="Filter on Event"
                    />
                  </div>
                  <div>
                    {this.state.eventArr.map(events => (
                      <div
                        onClick={e => {
                          console.log(events.E_CODE + " in div onclick");
                          this.setState({ showRegButton: true });
                          this.setState({ application_status: "Register" });
                          this.displayEvent(events.E_CODE);
                        }}
                        className="e mb-3"
                      >
                        <div
                          style={{
                            fontWeight: "500",
                            fontSize: "15px"
                          }}
                        >
                          {events.E_NAME}
                        </div>

                        <div
                          style={{
                            fontSize: "13px",
                            color: "rgba(0,0,0,.56)"
                          }}
                        >
                          {events.DATE}
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
              </Card>
            </Col>
            <Col md={6}>
              {this.state.eventObj ? (
                <div>
                  <Card
                    bg="light"
                    className="mt-3 d-flex p-2"
                    style={{
                      width: "auto",
                      marginBottom: "20px",
                      height: "500px"
                    }}
                  >
                    <div className="style__jobs___3seWY p-2">
                      <div style={{ fontSize: "24px", fontWeight: "500" }}>
                        {this.state.eventObj.E_NAME}
                      </div>

                      <div>
                        <div className="d-flex">
                          <ion-icon
                            style={{
                              opacity: "0.6"
                            }}
                            name="calendar-outline"
                          />

                          <div
                            style={{
                              left: "15px",
                              marginLeft: "5px",
                              fontSize: "13px",
                              color: "rgba(0,0,0,.56)"
                            }}
                          >
                            {this.state.eventObj.DATE},{" "}
                            {this.state.eventObj.TIME} PDT
                          </div>
                        </div>
                        <div className="d-flex">
                          <ion-icon
                            style={{
                              opacity: "0.6"
                            }}
                            name="eye-outline"
                          />

                          <div
                            style={{
                              left: "15px",
                              marginLeft: "5px",
                              fontSize: "13px",
                              color: "rgba(0,0,0,.56)"
                            }}
                          >
                            Any Handshake student with a link to this event can
                            view and RSVP
                          </div>
                        </div>
                      </div>
                      <div className="style__card___31yrn mt-2 d-flex justify-content-between">
                        <div className="style__large___3qwwG mt-2">
                          RSVP for the Event here{" "}
                        </div>

                        {this.state.showRegButton ? this.state
                          .application_status == "Register" ? (
                          <button
                            onClick={this.studentApply}
                            type="button"
                            className="btn btn-outline-danger style__base___hEhR9"
                          >
                            <div className="d-flex">
                              <div
                                style={{
                                  top: "2px",
                                  position: "relative"
                                }}
                              >
                                <ion-icon name="add-outline" />
                              </div>

                              <span>{this.state.application_status}</span>
                              <span style={{ fontWeight: "bold" }} />
                            </div>
                          </button>
                        ) : (
                          <button
                            onClick={this.studentApply}
                            type="button"
                            className="btn btn-danger style__base___hEhR9"
                          >
                            <div className="d-flex">
                              <div
                                style={{
                                  top: "2px",
                                  position: "relative"
                                }}
                              />

                              <span>{this.state.application_status}</span>
                              <span style={{ fontWeight: "bold" }} />
                            </div>
                          </button>
                        ) : (
                          <div />
                        )}
                      </div>
                      <div>{this.state.eventObj.E_DESC}</div>
                    </div>
                  </Card>
                </div>
              ) : (
                <div />
              )}
            </Col>
            <Col md={3}>
              <Card
                bg="light"
                className="mt-3 ml-3 d-flex"
                style={{ width: "auto", marginBottom: "20px", height: "500px" }}
              >
                <div className="style__jobs___3seWY p-2">
                  <h4 className="card-title mb-4 p-3" align="center">
                    Your Registered Events
                  </h4>
                  <div>
                    {this.state.eventRegArr.map(events => (
                      <div
                        onClick={e => {
                          console.log(events.E_CODE + " in div onclick");
                          this.displayEvent(events.E_CODE);
                          this.setState({ showRegButton: false });
                        }}
                        className="e mb-3"
                      >
                        <div
                          style={{
                            fontWeight: "500",
                            fontSize: "15px"
                          }}
                        >
                          {events.E_NAME}
                        </div>

                        <div
                          style={{
                            fontSize: "13px",
                            color: "rgba(0,0,0,.56)"
                          }}
                        >
                          {events.DATE}
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default StudentEvents;
