import React, { Component } from "react";
import { Route } from "react-router-dom";
import Navigation from "./LandingPage/Navbar";
import Login from "./Login/Login";
import RegisterStudent from "./RegisterStudent/RegisterStudent";
import RegisterCompany from "./RegisterCompany/RegisterCompany";
import StudentProfile from "./StudentProfile/StudentProfile";
import StudentJob from "./StudentJob/StudentJob";
import StudentEvents from "./StudentEvents/StudentEvents";
import StudentApplications from "./StudentApplications/StudentApplications";
import StudentAll from "./StudentAll/StudentAll";
import StudentSelectedProfile from "./StudentAll/StudentSelectedProfile";
import CompanyProfile from "./CompanyProfile/CompanyProfile";
import CompanyJobsStudents from "./CompanyProfile/CompanyJobsStudents";
import CompanyEvents from "./CompanyProfile/CompanyEvents";
import CompanyEventsStudents from "./CompanyProfile/CompanyEventsStudents";
import CompanySelectedProfile from "./CompanyAll/CompanySelectedProfile";

//Create a Main Component
class Main extends Component {
  render() {
    return (
      <div>
        <Route path="/" component={Navigation} />
        <Route path="/login" component={Login} />
        <Route path="/registerStudent" component={RegisterStudent} />
        <Route path="/registerCompany" component={RegisterCompany} />
        <Route path="/student/profile" component={StudentProfile} />
        <Route path="/student/job_list" component={StudentJob} />
        <Route path="/student/event_list" component={StudentEvents} />
        <Route path="/student/applications" component={StudentApplications} />
        <Route path="/student/all_students" component={StudentAll} />
        <Route
          path="/student/selected_student/:sid"
          component={StudentSelectedProfile}
        />
        <Route path="/company/profile" component={CompanyProfile} />
        <Route
          path="/company/job/students/:job_code"
          component={CompanyJobsStudents}
        />
        <Route path="/company/event_list" component={CompanyEvents} />
        <Route
          path="/company/event/students/:event_code"
          component={CompanyEventsStudents}
        />
        <Route path="/company/all_students" component={StudentAll} />
        <Route
          path="/company/selected_company/:job_code"
          component={CompanySelectedProfile}
        />
      </div>
    );
  }
}
//Export The Main Component
export default Main;
