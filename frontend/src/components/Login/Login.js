import React, { Component } from "react";
import "../../App.css";
import axios from "axios";
import cookie from "react-cookies";
import { Redirect } from "react-router";
// import { connect } from "react-redux";
// import { login } from "./../../actions/loginAction";
import { graphql } from "react-apollo";
import { loginMutation } from "../../mutation/mutation";

//Define a Login Component
class Login extends Component {
  //call the constructor method
  constructor(props) {
    //Call the constrictor of Super class i.e The Component
    super(props);
    //maintain the state required for this component
    this.state = {
      category: "student",
      email: "",
      password: "",
      authFlag: false,
      errorNumber: "",
      res: "",
      inLogin: true,
      loginFlag: "",
      status: "",
      message: ""
    };
    //Bind the handlers to this class
    this.categoryChangeHandler = this.categoryChangeHandler.bind(this);
    this.emailChangeHandler = this.emailChangeHandler.bind(this);
    this.passwordChangeHandler = this.passwordChangeHandler.bind(this);
    this.submitLogin = this.submitLogin.bind(this);
  }
  //Call the Will Mount to set the auth Flag to false
  // componentDidMount() {
  //   this.setState({
  //     authFlag: false
  //   });
  //   this.setState({
  //     loginFlag: this.props.loginFlag
  //   });
  // }

  //category change handler to update state variable with the text entered by the user
  categoryChangeHandler = e => {
    console.log("inside category change handler");
    this.setState({
      category: e.target.value
    });
  };

  //email change handler to update state variable with the text entered by the user
  emailChangeHandler = e => {
    e.preventDefault();
    console.log("inside email change handler");
    this.setState({
      email: e.target.value
    });
  };
  //password change handler to update state variable with the text entered by the user
  passwordChangeHandler = e => {
    e.preventDefault();
    console.log("inside password change handler");
    this.setState({
      password: e.target.value
    });
  };
  //submit Login handler to send a request to the node backend
  submitLogin = async e => {
    e.preventDefault();
    window.localStorage.setItem("category", this.state.category);
    let mutationResponse = await this.props.loginMutation({
      variables: {
        category: this.state.category,
        email_id: this.state.email,
        password: this.state.password
      }
    });
    console.log(mutationResponse);
    let response = mutationResponse.data.login;
    console.log(response);
    if (response) {
      if (response.status === "200") {
        console.log("in if");
        this.setState(
          {
            id: response.message,
            loginFlag: true
          },
          () => {
            console.log(this.state);
          }
        );
      } else {
        this.setState({
          res: response.message,
          loginFlag: false
        });
      }
    }
  };
  // submitLogin = e => {
  //   console.log("inside submit Login handler");
  //   //prevent page from refresh
  //   e.preventDefault();
  //   window.localStorage.setItem("category", this.state.category);

  //   const userData = {
  //     category: this.state.category,
  //     email: this.state.email,
  //     password: this.state.password
  //   };
  //   console.log(userData);

  //   this.props.login(userData);
  // };

  render() {
    //redirect based on successful login
    let redirectVar = null;
    let printError = null;
    console.log(this.state);
    if (!this.state.loginFlag) {
      printError = this.state.res;
      console.log("Error is : ", printError);
    } else {
      console.log("logged in");
      localStorage.removeItem("SID");
      localStorage.setItem("SID", this.state.id);
      localStorage.setItem("loginFlag", "true");
      if (this.state.category === "student") {
        redirectVar = <Redirect to="/student/profile" />;
      } else {
        redirectVar = <Redirect to="/company/profile" />;
      }
    }
    return (
      <div>
        {redirectVar}
        <div className="container">
          <div className="login-form">
            <div className="main-div">
              <div className="panel">
                <h2>Login</h2>
                <p>Please enter your email and password</p>
              </div>
              <div className="form-group">
                <p>
                  <label>Select category</label>
                  <select
                    value={this.state.category}
                    onChange={e => {
                      console.log("inside category set state");
                      this.setState({
                        category: e.target.value
                      });
                      console.log("category:e.target.value", e.target.value);
                    }}
                    id="myList"
                  >
                    <option value="student" name="student">
                      Student
                    </option>
                    <option value="company" name="company">
                      Company
                    </option>
                  </select>
                </p>
              </div>
              <div className="form-group">
                <input
                  onChange={this.emailChangeHandler}
                  type="text"
                  className="form-control"
                  name="email"
                  placeholder="Email"
                />
              </div>
              <div className="form-group">
                <input
                  onChange={this.passwordChangeHandler}
                  type="password"
                  className="form-control"
                  name="password"
                  placeholder="Password"
                />
              </div>
              <button onClick={this.submitLogin} className="btn btn-danger">
                Login
              </button>
              <h3 style={{ color: "red" }}>{printError}</h3>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

// const mapStateToProps = state => {
//   console.log("Login id in mapStateToProps ", state.loginReducer.id);
//   return {
//     id: state.loginReducer.id,
//     res: state.loginReducer.res,
//     loginFlag: state.loginReducer.loginFlag,
//     category: state.loginReducer.category,
//     name: state.loginReducer.name
//   };
// };
// const mapDispatchToProps = dispatch => {
//   return {
//     login: payload => dispatch(login(payload))
//   };
// };

//export Login Component
export default graphql(loginMutation, { name: "loginMutation" })(Login);
