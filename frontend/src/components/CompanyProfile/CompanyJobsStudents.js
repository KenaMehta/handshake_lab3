import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
import "../styles/job.css";
// import { connect } from "react-redux";
import cookie from "react-cookies";
import { Redirect } from "react-router";
import axios from "axios";
import configPath from "./../../configApp";
import { graphql, withApollo, compose } from "react-apollo";
import { getJobStudentsQuery } from "./../../queries/queries";

class CompanyJobsStudents extends Component {
  constructor(props) {
    super(props);
    this.state = {
      category_filter: "none",
      job_filter: "none",
      location_filter: "none",
      company_filter: "none",
      modalBox: "HideBox",
      studentArr: [],
      redirect: false,
      status: "Pending",
      status_line: "",
      selectedSID: ""
    };
  }

  //Set redirect flag to true
  setRedirect = () => {
    this.setState({
      redirect: true
    });
  };

  //update student's status
  updateStatus = sid => {
    // console.log("in updateStatus");
    // axios
    //   .put(configPath.api_host + `/company/job/students`, {
    //     status: this.state.status,
    //     sid: sid,
    //     job_id: localStorage.getItem("Selected_Job")
    //   })
    //   .then(response => {
    //     if (response.status === 200) {
    //       console.log("status_line updated");
    //       this.setState({ status_line: "Updated" });
    //     } else this.setState({ status_line: "Failed to update" });
    //   })
    //   .catch(error => this.setState({ status_line: "Failed to update" }));
  };

  //re-direct to student list
  renderRedirect = () => {
    const redirectPath = "/student/selected_student/" + this.state.selectedSID;
    if (this.state.redirect) {
      return <Redirect to={redirectPath} />;
    }
  };

  //Get Job List
  // getPage = () => {
  //   console.log(localStorage.getItem("Selected_Job"));
  //   axios
  //     .get(
  //       configPath.api_host +
  //         `/company/job/students/${localStorage.getItem("Selected_Job")}`
  //     )
  //     .then(response => {
  //       if (response.status === 200) {
  //         console.log(response.data);
  //         this.setState({ studentArr: response.data });
  //       }
  //     })
  //     .catch(err => console.log(err + " : Error in getting Profile details"));
  // };

  // //intermediate function to get the job list
  // callGetPage = () => {
  //   console.log(this.state.category_filter + "filter");
  //   this.getPage();
  // };

  //render job list
  // componentDidMount() {
  // console.log("In componentDidMount");
  // this.getPage();
  // }
  async componentWillReceiveProps(nextProps) {
    let results = await nextProps.getJobStudentsQuery;
    if (results && results.getJobStudents) {
      console.log(results.getJobStudents.studentList);
      this.setState({
        studentArr: results.getJobStudents.studentList
      });
    }
  }
  render() {
    // check if user is logged in
    // if (!cookie.load("cookie")) {
    //   return <Redirect to="/login" />;
    // }
    if (localStorage.getItem("loginFlag") != "true") {
      return <Redirect to="/login" />;
    }
    return (
      <div>
        {this.renderRedirect()}
        <Container>
          <Row>
            <Col md={12}>
              <Card
                border="danger"
                bg="light"
                className="card_style d-flex m-3"
                style={{ width: "auto" }}
              >
                <Col align="center" className="head_line" md={12}>
                  <h5 style={{ opacity: 0.7 }}>Students</h5>
                </Col>

                <div className="style__divider___1j_Fp mb-2" />
                <div className="style__jobs___3seWY p-2">
                  <div>
                    {this.state.studentArr ? (
                      <div>
                        <div>
                          {this.state.studentArr.map(student => (
                            <Card
                              bg="light"
                              className="e card_style m-3 p-2"
                              style={{
                                width: "auto",
                                marginBottom: "20px",
                                height: "auto"
                              }}
                            >
                              <div className="d-flex">
                                <div className="p-2 col-2">
                                  {student.SID.PHOTO != null ? (
                                    <img
                                      style={{
                                        border: "1px solid #ddd",
                                        borderRadius: "4px",
                                        padding: "5px",
                                        width: "70px"
                                      }}
                                      src={
                                        `${configPath.base}/images/` +
                                        student.SID.PHOTO
                                      }
                                    />
                                  ) : (
                                    <div className="mt-3 ml-3">
                                      <ion-icon
                                        size="large"
                                        name="person-outline"
                                      />
                                    </div>
                                  )}
                                </div>
                                <div
                                  className="p-2 col-4"
                                  onClick={e => {
                                    console.log(
                                      student.SID._id + " in div onclick"
                                    );
                                    localStorage.setItem(
                                      "Selected_SID",
                                      student.SID._id
                                    );
                                    this.setState(
                                      { selectedSID: student.SID._id },
                                      () => this.setRedirect()
                                    );
                                  }}
                                >
                                  <div
                                    style={{
                                      fontWeight: "600",
                                      fontSize: "18px"
                                    }}
                                  >
                                    {student.SID.NAME}
                                  </div>
                                  {student.SID.EDUCATION.length != 0 ? (
                                    student.SID.EDUCATION.map(
                                      edu =>
                                        student.SID.COLLEGE_NAME ==
                                        edu.COLLEGE_NAME ? (
                                          <div>
                                            <div
                                              style={{
                                                fontWeight: "500",
                                                fontSize: "14px"
                                              }}
                                            >
                                              {edu.COLLEGE_NAME}
                                            </div>
                                            <div
                                              style={{
                                                fontWeight: "400",
                                                fontSize: "13px"
                                              }}
                                            >
                                              {edu.DEGREE}, Year of Passing:{" "}
                                              {edu.YEAR_OF_PASSING} |{" "}
                                              {edu.MAJOR}
                                            </div>
                                          </div>
                                        ) : (
                                          ""
                                        )
                                    )
                                  ) : (
                                    <div
                                      style={{
                                        fontWeight: "500",
                                        fontSize: "14px"
                                      }}
                                    >
                                      {student.SID.COLLEGE_NAME}
                                    </div>
                                  )}
                                </div>
                                <div className="p-2 col-4">
                                  {console.log(student.RESUME)}
                                  {student.RESUME ? (
                                    <a
                                      href={
                                        `${configPath.base}/images/` +
                                        `${student.RESUME}`
                                      }
                                      download="Resume"
                                      target="_blank"
                                    >
                                      View Resume
                                    </a>
                                  ) : (
                                    ""
                                  )}
                                </div>
                                <div className="">
                                  <div className="ml-3">
                                    <select
                                      value={this.state.status}
                                      id="status"
                                      onChange={e => {
                                        this.setState({
                                          status: e.target.value
                                        });
                                        this.props.refreshStatus({
                                          status_line: ""
                                        });
                                      }}
                                    >
                                      <option>Pending</option>
                                      <option>Reviewed</option>
                                      <option>Accepted</option>
                                      <option>Declined</option>
                                    </select>
                                  </div>
                                  <div>
                                    <button
                                      style={{
                                        borderRadius: "20px",
                                        fontSize: "13px"
                                      }}
                                      type="button"
                                      className="btn btn-outline-secondary m-2"
                                      onClick={e => {
                                        this.updateStatus(student.SID._id);
                                      }}
                                    >
                                      Update Status
                                    </button>
                                  </div>
                                </div>
                              </div>
                            </Card>
                          ))}
                        </div>
                        <div />
                      </div>
                    ) : (
                      <div />
                    )}
                  </div>
                </div>
              </Card>
              <Card className={this.state.modalBox + " modal"}>
                <div className="modal-content col-5">
                  <Container>
                    <span className="close" onClick={this.modalUpdate}>
                      &times;
                    </span>
                    <form onSubmit={this.addJob}>
                      <div className="form-group">
                        <label style={{ fontWeight: "bold" }}>Job Title</label>
                        <input
                          name="job_title"
                          onChange={e => {
                            this.setState({ JOB_TITLE: e.target.value });
                          }}
                          type="text"
                          placeholder="Enter Job Title"
                          className="form-control"
                        />

                        <div className="mb-3" style={{ fontWeight: "bold" }}>
                          Job Category
                        </div>
                        <select
                          className="form-control"
                          value={this.state.JOB_CATEGORY}
                          id="category"
                          onChange={e => {
                            this.setState({ JOB_CATEGORY: e.target.value });
                          }}
                          required
                        >
                          <option value="full-time">Full Time</option>
                          <option value="part-time">Part Time</option>
                          <option value="on-campus">On Campus</option>
                          <option value="internship">Internship</option>
                        </select>

                        <div className="d-flex justify-content-between">
                          <div className="form-group">
                            <label style={{ fontWeight: "bold" }}>Salary</label>
                            <input
                              name="Salary"
                              onChange={e =>
                                this.setState({ SALARY: e.target.value })}
                              type="number"
                              placeholder="Enter Salary"
                              className="form-control"
                            />
                          </div>
                          <div className="form-group">
                            <label style={{ fontWeight: "bold" }}>
                              Application Deadline
                            </label>
                            <input
                              name="app_deadline"
                              onChange={e =>
                                this.setState({ APP_DEADLINE: e.target.value })}
                              type="date"
                              className="form-control"
                            />
                          </div>
                        </div>
                        <div className="d-flex justify-content-between">
                          <div className="form-group">
                            <label style={{ fontWeight: "bold" }}>City</label>
                            <input
                              name="city"
                              onChange={e =>
                                this.setState({ CITY: e.target.value })}
                              type="text"
                              placeholder="Enter City"
                              className="form-control"
                            />
                          </div>
                          <div className="form-group">
                            <label style={{ fontWeight: "bold" }}>State</label>
                            <input
                              name="state"
                              onChange={e =>
                                this.setState({ STATE: e.target.value })}
                              type="text"
                              placeholder="Enter State"
                              className="form-control"
                            />
                          </div>
                          <div className="form-group">
                            <label style={{ fontWeight: "bold" }}>
                              Country
                            </label>
                            <input
                              name="country"
                              onChange={e =>
                                this.setState({ COUNTRY: e.target.value })}
                              type="text"
                              placeholder="Enter Country"
                              className="form-control"
                            />
                          </div>
                        </div>
                        <div className="form-group">
                          <label style={{ fontWeight: "bold" }}>
                            Job Description
                          </label>
                          <textarea
                            name="job_desc"
                            onChange={e =>
                              this.setState({ JOB_DESC: e.target.value })}
                            type="number"
                            placeholder="Enter Job Desc"
                            className="form-control"
                          />
                        </div>

                        <div />
                        <div className="m-3" align="center">
                          <input type="submit" value="Submit" />

                          <span style={{ fontWeight: "bold", color: "red" }} />
                        </div>
                      </div>
                    </form>
                  </Container>
                </div>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
export default graphql(getJobStudentsQuery, {
  name: "getJobStudentsQuery",
  options: () => {
    return {
      variables: {
        jid: localStorage.getItem("Selected_Job")
      }
    };
  }
})(CompanyJobsStudents);
