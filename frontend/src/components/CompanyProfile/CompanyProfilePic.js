import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
// import { connect } from "react-redux";
import { getCompanyProfileQuery } from "./../../queries/queries";
import { updateCompanyProfileMutation } from "./../../mutation/mutation";
import { graphql, compose } from "react-apollo";
import { Redirect } from "react-router";
import axios from "axios";
import configPath from "./../../configApp";
// import Dropzone from "react-dropzone";
// import request from "superagent";

class CompanyProfilePic extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      location: "",
      description: "",
      phone: "",
      profileUpdateForm: "HideForm",
      profileObj: {},
      picture: "",
      profilePicture: ""
    };
  }

  updateProfilePic = () => {
    console.log(this.state);
    if (this.state.profileUpdateForm == "DisplayForm")
      this.setState({ profileUpdateForm: "HideForm" });
    else this.setState({ profileUpdateForm: "DisplayForm" });
  };

  companyDetailsUpdate = async e => {
    e.preventDefault();
    const dataAdd = {
      NAME: this.state.name,
      LOCATION: this.state.location,
      DESCRIPTION: this.state.description,
      PHONE: this.state.phone
    };
    let mutationResponse = await this.props.updateCompanyProfileMutation({
      variables: {
        ...dataAdd,
        cid: localStorage.getItem("SID")
      },
      refetchQueries: [
        {
          query: getCompanyProfileQuery,
          variables: { cid: localStorage.getItem("SID") }
        }
      ]
    });
    // axios
    //   .post(
    //     configPath.api_host + "/company/profile/" + localStorage.getItem("SID"),
    //     dataAdd
    //   )
    //   .then(res => {
    //     if (res.status === 200) {
    //       console.log(res.data);
    //       this.setState({
    //         profileObj: res.data
    //       });
    //       localStorage.setItem("company_name", res.data.NAME);
    //     }
    //   })
    //   .catch(err => console.log(err + " err in adding/updating"));
    this.updateProfilePic();
  };
  // componentDidMount() {
  //   console.log(
  //     "in com did mount of profile piic localStorage.getItem sid " +
  //       configPath.api_host +
  //       "/company/profile/" +
  //       localStorage.getItem("SID")
  //   );
  //   setTimeout(() => {
  //     axios
  //       .get(
  //         configPath.api_host +
  //           "/company/profile/" +
  //           localStorage.getItem("SID")
  //       )
  //       .then(response => {
  //         if (response.status === 200) {
  //           localStorage.setItem("company_name", response.data.NAME);
  //           console.log(
  //             JSON.stringify(response.data) + " : Axios Profile response"
  //           );
  //           var path =
  //             `${configPath.base}/images/` + `${response.data.PROFILE_PIC}`;
  //           this.setState({ profilePicture: path });
  //           this.setState({ profileObj: response.data });
  //           console.log(
  //             JSON.stringify(this.state.profileObj) + " state after axios call"
  //           );
  //         }
  //       })
  //       .catch(err => console.log(err + " : Error in getting Profile details"));
  //   }, 500);
  // }
  // updatePic = e => {
  //   e.preventDefault();
  //   let picdata = new FormData();
  //   picdata.append("myimage", this.state.picture);
  //   picdata.append("name", "profile");

  //   console.log("mounting in picture------------");
  //   console.log(this.state.picture);

  //   try {
  //     console.log("In try block");
  //     axios
  //       .post(
  //         configPath.api_host +
  //           `/company/profile/picture/${localStorage.getItem("SID")}`,
  //         picdata
  //       )
  //       .then(res => {
  //         console.log(res.data);
  //         var path = `${configPath.base}/images/` + `${res.data}`;
  //         console.log(path);
  //         this.setState({ profilePicture: path });
  //       })
  //       .catch(err => {
  //         console.log(err);
  //       });
  //   } catch (err) {
  //     console.log(err);
  //   }
  // };
  componentWillReceiveProps(nextProps) {
    let results = nextProps.getCompanyProfileQuery;
    if (results && results.getCompanyProfile) {
      this.setState({ profileObj: results.getCompanyProfile });
      localStorage.setItem("company_name", results.getCompanyProfile.NAME);
    }
  }
  render() {
    return (
      <div>
        <Card
          className="card_style"
          border="danger"
          bg="light"
          style={{ width: "18rem" }}
          align="center"
        >
          <img
            style={{
              border: "1px solid #ddd",
              borderRadius: "4px",
              padding: "5px",
              width: "150px"
            }}
            src={`${configPath.base}/images/${this.state.profileObj
              .PROFILE_PIC}`}
          />

          <Card.Body>
            <Card.Title>{this.state.profileObj.NAME}</Card.Title>

            <Card.Text>{this.state.profileObj.LOCATION}</Card.Text>
            <Card.Text>{this.state.profileObj.PHONE}</Card.Text>
            <Card.Text style={{ color: "rgba(0, 0, 0, 0.56)" }}>
              {this.state.profileObj.DESCRIPTION}
            </Card.Text>
            <Button
              className="btn-danger"
              onClick={this.updateProfilePic}
              variant="primary"
            >
              Update
            </Button>
          </Card.Body>
        </Card>
        <Card
          bg="light"
          style={{ width: "18rem" }}
          className={this.state.profileUpdateForm + " card_style edu-form"}
        >
          <Card.Body>
            <form>
              <div className="form-group">
                <label style={{ fontWeight: "bold" }}>Name: </label>
                <input
                  name="name"
                  onChange={e => {
                    this.setState({ name: e.target.value });
                  }}
                  type="text"
                  placeholder="Edit Company Name"
                  className="form-control"
                />
              </div>
              <div className="form-group">
                <label style={{ fontWeight: "bold" }}>Location</label>
                <input
                  name="city"
                  onChange={e => this.setState({ location: e.target.value })}
                  type="text"
                  placeholder="Enter City"
                  className="form-control"
                />
              </div>
              <div className="form-group">
                <label style={{ fontWeight: "bold" }}>Phone</label>
                <input
                  name="state"
                  onChange={e => this.setState({ phone: e.target.value })}
                  type="number"
                  placeholder="Enter 10 digit number"
                  className="form-control"
                />
              </div>

              <div className="form-group">
                <label style={{ fontWeight: "bold" }}>Description</label>
                <textarea
                  name="country"
                  onChange={e => this.setState({ description: e.target.value })}
                  type="text"
                  placeholder="Enter Description"
                  className="form-control"
                />
              </div>
            </form>
            <Button
              className="btn-danger"
              onClick={this.companyDetailsUpdate}
              variant="primary"
            >
              {" "}
              Update
            </Button>

            <Button
              className="btn-secondary ml-3"
              onClick={() => {
                this.setState({ profileUpdateForm: "HideForm" });
              }}
              variant="primary"
            >
              {" "}
              Cancel
            </Button>
          </Card.Body>
        </Card>
      </div>
    );
  }
}

// export default CompanyProfilePic;
export default compose(
  graphql(getCompanyProfileQuery, {
    name: "getCompanyProfileQuery",
    options: () => {
      return {
        variables: {
          cid: localStorage.getItem("SID")
        }
      };
    }
  }),
  graphql(updateCompanyProfileMutation, {
    name: "updateCompanyProfileMutation"
  })
)(CompanyProfilePic);
