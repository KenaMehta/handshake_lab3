import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
import "../styles/job.css";
import { connect } from "react-redux";
import cookie from "react-cookies";
import { Redirect } from "react-router";
import axios from "axios";
import configPath from "./../../configApp";

class CompanyEventsStudents extends Component {
  constructor(props) {
    super(props);
    this.state = {
      category_filter: "none",
      job_filter: "none",
      location_filter: "none",
      company_filter: "none",
      modalBox: "HideBox",
      studentArr: [],
      redirect: false
    };
  }

  setRedirect = () => {
    this.setState({
      redirect: true
    });
  };
  renderRedirect = () => {
    const redirectPath = "/student/selected_student/" + this.state.selectedSID;
    if (this.state.redirect) {
      return <Redirect to={redirectPath} />;
    }
  };

  getPage = () => {
    console.log(localStorage.getItem("Selected_Event"));
    axios
      .get(
        configPath.api_host +
          `/company/event/students/${localStorage.getItem("Selected_Event")}`
      )
      .then(response => {
        if (response.status === 200) {
          console.log(response.data);
          this.setState({ studentArr: response.data });
        }
      })
      .catch(err => console.log(err + " : Error in getting Profile details"));
  };
  callGetPage = () => {
    console.log(this.state.category_filter + "filter");
    this.getPage();
  };

  componentDidMount() {
    console.log("In componentDidMount");
    this.getPage();
  }

  render() {
    // if (!cookie.load("cookie")) {
    //   return <Redirect to="/login" />;
    // }
    if (localStorage.getItem("loginFlag") != "true") {
      return <Redirect to="/login" />;
    }
    return (
      <div>
        {this.renderRedirect()}
        <Container>
          <Row>
            <Col md={12}>
              <Card
                border="danger"
                bg="light"
                className="card_style d-flex m-3"
                style={{ width: "auto" }}
              >
                <Col align="center" className="head_line" md={12}>
                  <h5 style={{ opacity: 0.7 }}>Event/Students</h5>
                </Col>

                <div className="style__divider___1j_Fp mb-2" />
                <div className="style__jobs___3seWY p-2">
                  <div>
                    {this.state.studentArr ? (
                      <div>
                        <div>
                          {this.state.studentArr.map(student => (
                            <Card
                              bg="light"
                              className="e card_style m-3 p-2"
                              onClick={e => {
                                console.log(student.SID + " in div onclick");
                                localStorage.setItem(
                                  "Selected_SID",
                                  student.SID
                                );
                                this.setState(
                                  { selectedSID: student.SID },
                                  () => this.setRedirect()
                                );
                              }}
                              style={{
                                width: "auto",
                                marginBottom: "20px",
                                height: "auto"
                              }}
                            >
                              <div
                                style={{
                                  fontWeight: "600",
                                  fontSize: "18px"
                                }}
                              >
                                {student.NAME}
                              </div>
                              <div
                                style={{
                                  fontWeight: "500",
                                  fontSize: "14px"
                                }}
                              >
                                {student.COLLEGE_NAME}
                              </div>
                              <div
                                style={{
                                  fontWeight: "400",
                                  fontSize: "13px"
                                }}
                              >
                                {student.DEGREE}, Year of Passing:{" "}
                                {student.YEAR_OF_PASSING} | {student.MAJOR}
                              </div>
                            </Card>
                          ))}
                        </div>
                      </div>
                    ) : (
                      <div />
                    )}
                  </div>
                </div>
              </Card>
              <Card className={this.state.modalBox + " modal"}>
                <div className="modal-content col-5">
                  <Container>
                    <span className="close" onClick={this.modalUpdate}>
                      &times;
                    </span>
                    <form onSubmit={this.addJob}>
                      <div className="form-group">
                        <label style={{ fontWeight: "bold" }}>Job Title</label>
                        <input
                          name="job_title"
                          onChange={e => {
                            this.setState({ JOB_TITLE: e.target.value });
                          }}
                          type="text"
                          placeholder="Enter Job Title"
                          className="form-control"
                        />

                        <div className="mb-3" style={{ fontWeight: "bold" }}>
                          Job Category
                        </div>
                        <select
                          className="form-control"
                          value={this.state.JOB_CATEGORY}
                          id="category"
                          onChange={e => {
                            this.setState({ JOB_CATEGORY: e.target.value });
                          }}
                          required
                        >
                          <option value="full-time">Full Time</option>
                          <option value="part-time">Part Time</option>
                          <option value="on-campus">On Campus</option>
                          <option value="internship">Internship</option>
                        </select>

                        <div className="d-flex justify-content-between">
                          <div className="form-group">
                            <label style={{ fontWeight: "bold" }}>Salary</label>
                            <input
                              name="Salary"
                              onChange={e =>
                                this.setState({ SALARY: e.target.value })}
                              type="number"
                              placeholder="Enter Salary"
                              className="form-control"
                            />
                          </div>
                          <div className="form-group">
                            <label style={{ fontWeight: "bold" }}>
                              Application Deadline
                            </label>
                            <input
                              name="app_deadline"
                              onChange={e =>
                                this.setState({ APP_DEADLINE: e.target.value })}
                              type="date"
                              className="form-control"
                            />
                          </div>
                        </div>
                        <div className="d-flex justify-content-between">
                          <div className="form-group">
                            <label style={{ fontWeight: "bold" }}>City</label>
                            <input
                              name="city"
                              onChange={e =>
                                this.setState({ CITY: e.target.value })}
                              type="text"
                              placeholder="Enter City"
                              className="form-control"
                            />
                          </div>
                          <div className="form-group">
                            <label style={{ fontWeight: "bold" }}>State</label>
                            <input
                              name="state"
                              onChange={e =>
                                this.setState({ STATE: e.target.value })}
                              type="text"
                              placeholder="Enter State"
                              className="form-control"
                            />
                          </div>
                          <div className="form-group">
                            <label style={{ fontWeight: "bold" }}>
                              Country
                            </label>
                            <input
                              name="country"
                              onChange={e =>
                                this.setState({ COUNTRY: e.target.value })}
                              type="text"
                              placeholder="Enter Country"
                              className="form-control"
                            />
                          </div>
                        </div>
                        <div className="form-group">
                          <label style={{ fontWeight: "bold" }}>
                            Job Description
                          </label>
                          <textarea
                            name="job_desc"
                            onChange={e =>
                              this.setState({ JOB_DESC: e.target.value })}
                            type="number"
                            placeholder="Enter Job Desc"
                            className="form-control"
                          />
                        </div>

                        <div />
                        <div className="m-3" align="center">
                          <input type="submit" value="Submit" />

                          <span style={{ fontWeight: "bold", color: "red" }} />
                        </div>
                      </div>
                    </form>
                  </Container>
                </div>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
export default CompanyEventsStudents;
