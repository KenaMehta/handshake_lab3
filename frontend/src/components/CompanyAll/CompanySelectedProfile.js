import React, { Component } from "react";
import CompanySelectedProfilePic from "./CompanySelectedProfilePic";
import CompanySelectedJobs from "./CompanySelectedJobs";
import cookie from "react-cookies";
import { connect } from "react-redux";
import { Redirect } from "react-router";

import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
class CompanySelectedProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      companyProfilePic: "",
      companyJobs: ""
    };
  }
  componentDidMount() {
    this.setState({ companyProfilePic: <CompanySelectedProfilePic /> });
    setTimeout(() => {
      this.setState({ companyJobs: <CompanySelectedJobs /> });
    }, 200);
  }
  render() {
    // if (!cookie.load("cookie")) {
    //   return <Redirect to="/login" />;
    // }
    if (localStorage.getItem("loginFlag") != "true") {
      return <Redirect to="/login" />;
    }

    return (
      <div>
        <Row align="center">
          <Col className="head_line" md={12}>
            <h2 style={{ opacity: 0.7 }}>Profile</h2>
          </Col>
        </Row>
        <Container>
          <Row>
            <Col md={4}>{this.state.companyProfilePic}</Col>
            <Col md={8}>{this.state.companyJobs}</Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default CompanySelectedProfile;
