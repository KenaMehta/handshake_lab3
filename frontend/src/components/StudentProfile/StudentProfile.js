import React, { Component } from "react";
import StudentProfilePic from "./StudentProfilePic";
import StudentSkills from "./StudentSkills";
import StudentJourney from "./StudentJourney";
import StudentEducation from "./StudentEducation";
import StudentExperience from "./StudentExperience";
import StudentBasicDetails from "./StudentBasicDetails";
import cookie from "react-cookies";
// import { connect } from "react-redux";
import { Redirect } from "react-router";
import { graphql } from "react-apollo";
import { getStudentQuery } from "../../queries/queries";

import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
class StudentProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    let studentProfilePic = "",
      studentSkills = "",
      studentBasicDetails = "",
      studentJourney = "",
      studentEducation = "",
      studentExperience = "";
    if (localStorage.getItem("loginFlag") != "true") {
      return <Redirect to="/login" />;
    }
    let results = this.props.getStudentQuery;
    if (results && results.student) {
      console.log(results.student);
      studentProfilePic = (
        <StudentProfilePic
          PHOTO={results.student.PHOTO}
          name={results.student.name}
          school={results.student.school}
          DEGREE={results.student.DEGREE}
          MAJOR={results.student.MAJOR}
          GPA={results.student.GPA}
        />
      );
      studentSkills = <StudentSkills skillArr={results.student.Skill} />;
      studentBasicDetails = <StudentBasicDetails detailObj={results.student} />;
      studentJourney = <StudentJourney journey={results.student.journey} />;
      studentEducation = (
        <StudentEducation educationArr={results.student.Education} />
      );
      studentExperience = (
        <StudentExperience experienceArr={results.student.Experience} />
      );
    }
    return (
      <div>
        <Row align="center">
          <Col className="head_line" md={12}>
            <h2 style={{ opacity: 0.7 }}>Your Profile</h2>
          </Col>
        </Row>
        <Container align="center">
          <Row>
            <Col md={4}>
              {studentProfilePic}
              {studentSkills}
              {studentBasicDetails}
            </Col>
            <Col md={8}>
              {studentJourney}
              {studentEducation}
              {studentExperience}
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default graphql(getStudentQuery, {
  name: "getStudentQuery",
  options: () => {
    return {
      variables: {
        sid: localStorage.getItem("SID")
      }
    };
  }
})(StudentProfile);
