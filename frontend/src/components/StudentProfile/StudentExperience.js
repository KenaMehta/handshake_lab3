import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
import axios from "axios";
import configPath from "./../../configApp";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import { getStudentQuery } from "./../../queries/queries";
import { updateStudentExperienceMutation } from "./../../mutation/mutation";
import { graphql, compose } from "react-apollo";

class StudentExperience extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profileUpdateForm: "HideForm",
      deleteExperience: "",
      experienceArr: [],
      deleteFlag: false,
      company_name: "",
      title: "",
      location: "",
      start_dt: "",
      end_dt: "",
      work_desc: ""
    };
  }
  experienceUpdate = () => {
    if (this.state.profileUpdateForm == "DisplayForm")
      this.setState({ profileUpdateForm: "HideForm" });
    else this.setState({ profileUpdateForm: "DisplayForm" });
  };
  experienceDelete = () => {
    console.log("code for delete");
    console.log(this.state.deleteExperience);
    let data = { deleteExperience: this.state.deleteExperience };
    axios
      .delete(
        configPath.api_host +
          `/student/profile/experience/${localStorage.getItem("SID")}`,
        { data: { data: data } }
      )
      .then(res => {
        console.log(JSON.stringify(res.data) + "---> obj after deleting");
        this.setState({ deleteFlag: false });
        if (res.status === 200) {
          this.setState({ experienceArr: res.data });
          console.log("Successfully Deleted");
        } else console.log("Error in deleting");
      })
      .catch(err => console.log("Error in catch: " + err));
    this.experienceUpdate();
  };
  experienceAdd = async e => {
    e.preventDefault();
    const dataAdd = {
      COMPANY_NAME: this.state.company_name,
      TITLE: this.state.title,
      LOCATION: this.state.location,
      START_DT: this.state.start_date,
      END_DT: this.state.end_date,
      WORK_DESC: this.state.work_desc
    };
    let mutationResponse = await this.props.updateStudentExperienceMutation({
      variables: {
        ...dataAdd,
        sid: localStorage.getItem("SID")
      },
      refetchQueries: [
        {
          query: getStudentQuery,
          variables: { sid: localStorage.getItem("SID") }
        }
      ]
    });
    this.experienceUpdate();

    // console.log("inside experience add");
    // const dataAdd = {
    //   COMPANY_NAME: this.state.company_name,
    //   TITLE: this.state.title,
    //   LOCATION: this.state.location,
    //   START_DT: this.state.start_date,
    //   END_DT: this.state.end_date,
    //   WORK_DESC: this.state.work_desc
    // };
    // axios
    //   .post(
    //     configPath.api_host +
    //       `/student/profile/experience/${localStorage.getItem("SID")}`,
    //     dataAdd
    //   )
    //   .then(res => {
    //     if (res.status === 200) {
    //       console.log(res.data);
    //       this.setState({
    //         experienceArr: res.data
    //       });
    //     }
    //   })
    //   .catch(err => console.log(err + " err in adding/updating experience"));
    // this.experienceUpdate();
  };
  // componentDidMount() {
  //   axios
  //     .get(
  //       configPath.api_host +
  //         `/student/profile/experience/${localStorage.getItem("SID")}`
  //     )
  //     .then(response => {
  //       if (response.status === 200) {
  //         console.log(
  //           JSON.stringify(response.data) + " : Axios Experience response"
  //         );
  //         this.setState({ experienceArr: response.data });
  //         console.log(this.state.experienceArr + " state after axios call");
  //       }
  //     })
  //     .catch(err =>
  //       console.log(err + " : Error in getting Experience details")
  //     );
  // }
  render() {
    return (
      <div>
        <Card className="card_style" border="danger" bg="light" align="left">
          <Card.Body>
            <Card.Title>Work and Volunteer Experience</Card.Title>
            <div className="style__divider___1j_Fp mb-3" />
            {this.props.experienceArr.map(experience => (
              <div style={{ marginTop: "20px" }}>
                <div style={{ fontSize: "18px" }}>
                  {experience.COMPANY_NAME}
                </div>
                <div
                  style={{
                    fontSize: "16px",
                    lineHeight: "24px",
                    color: "rgba(0,0,0,.8)"
                  }}
                >
                  {experience.TITLE}
                </div>
                <div
                  style={{
                    fontSize: "13px",
                    lineHeight: "20px",
                    color: "rgba(0,0,0,.8)"
                  }}
                >
                  {experience.START_DT} - {experience.END_DT} |{" "}
                  {experience.LOCATION}
                </div>
                <div
                  style={{
                    fontSize: "13px",
                    lineHeight: "20px"
                  }}
                >
                  {experience.WORK_DESC}
                </div>
              </div>
            ))}
            <Button
              className="btn-danger"
              style={{ marginTop: "20px", marginBottom: "10px" }}
              onClick={e => {
                this.experienceUpdate();
                this.setState({ deleteFlag: false });
              }}
              variant="primary"
            >
              Add/Update
            </Button>
            <Button
              className="btn-danger"
              style={{
                marginLeft: "20px",
                marginTop: "20px",
                marginBottom: "10px"
              }}
              onClick={e => {
                this.experienceUpdate();
                this.setState({ deleteFlag: true });
              }}
              variant="primary"
            >
              Delete
            </Button>
          </Card.Body>
        </Card>
        {this.state.deleteFlag ? (
          <Card
            bg="light"
            className={this.state.profileUpdateForm + " card_style edu-form"}
            align="left"
          >
            <Card.Body>
              <Card.Title>Delete Experience</Card.Title>
              <div className="content margin-top" style={{ margin: "20px" }}>
                <form>
                  <div className="form-group">
                    <label style={{ fontWeight: "bold" }}>Company name</label>
                    <input
                      name="company"
                      onChange={e => {
                        this.setState({ deleteExperience: e.target.value });
                      }}
                      type="text"
                      placeholder="Enter company you want to delete"
                      className="form-control"
                    />
                  </div>
                </form>
              </div>
              <Button
                className="btn-danger"
                style={{
                  marginLeft: "20px",
                  marginBottom: "10px"
                }}
                onClick={
                  this.state.deleteExperience !== "" ? (
                    this.experienceDelete
                  ) : (
                    this.experienceUpdate
                  )
                }
                variant="primary"
              >
                Delete
              </Button>
              <Button
                className="btn-danger"
                style={{
                  marginLeft: "20px",
                  marginBottom: "10px"
                }}
                onClick={this.experienceUpdate}
                variant="primary"
              >
                Cancel
              </Button>
            </Card.Body>
          </Card>
        ) : (
          <Card
            bg="light"
            className={this.state.profileUpdateForm + " card_style edu-form"}
            align="left"
          >
            <Card.Body>
              <Card.Title>Add Experience</Card.Title>
              <div className="content margin-top" style={{ margin: "20px" }}>
                <form>
                  <div className="form-group">
                    <label style={{ fontWeight: "bold" }}>Job Title</label>
                    <input
                      name="school"
                      onChange={e => this.setState({ title: e.target.value })}
                      type="text"
                      placeholder="Enter Company Name"
                      className="form-control"
                    />
                  </div>
                  <div className="form-group">
                    <label style={{ fontWeight: "bold" }}>Employer</label>
                    <input
                      name="name"
                      onChange={e =>
                        this.setState({ company_name: e.target.value })}
                      type="text"
                      placeholder="eg. Enter your employer"
                      className="form-control"
                    />
                  </div>
                  <label style={{ fontWeight: "bold" }}>Time Period</label>
                  <div className="form-group d-flex">
                    <div className="form-group">
                      <label>Start Date</label>
                      <input
                        name="start_date"
                        onChange={e =>
                          this.setState({ start_date: e.target.value })}
                        type="date"
                        className="form-control"
                      />
                    </div>
                    <div className="form-group ml-4">
                      <label>End Date</label>
                      <input
                        name="end_date"
                        onChange={e =>
                          this.setState({ end_date: e.target.value })}
                        type="date"
                        className="form-control"
                      />
                    </div>
                  </div>
                  <div className="form-group">
                    <label style={{ fontWeight: "bold" }}>Location</label>
                    <input
                      name="gpa"
                      onChange={e =>
                        this.setState({ location: e.target.value })}
                      type="text"
                      placeholder="Enter the city you worked in"
                      className="form-control"
                    />
                  </div>
                  <div className="form-group">
                    <label style={{ fontWeight: "bold" }}>
                      Work Description
                    </label>
                    <textarea
                      name="gpa"
                      onChange={e =>
                        this.setState({ work_desc: e.target.value })}
                      rows="4"
                      type="textarea"
                      placeholder="Describe your work here"
                      className="form-control"
                    />
                  </div>
                  <div />
                </form>
                <Button
                  className="btn-danger"
                  onClick={
                    this.state.school !== "" ? (
                      this.experienceAdd
                    ) : (
                      this.experienceUpdate
                    )
                  }
                  variant="primary"
                >
                  {" "}
                  Add
                </Button>
                <Button
                  className="btn-secondary ml-3"
                  onClick={() => {
                    this.setState({ profileUpdateForm: "HideForm" });
                  }}
                  variant="primary"
                >
                  {" "}
                  Cancel
                </Button>
              </div>
            </Card.Body>
          </Card>
        )}
      </div>
    );
  }
}

// export default StudentExperience;
export default compose(
  graphql(getStudentQuery, {
    options: {
      variables: { sid: localStorage.getItem("SID") }
    }
  }),
  graphql(updateStudentExperienceMutation, {
    name: "updateStudentExperienceMutation"
  })
)(StudentExperience);
