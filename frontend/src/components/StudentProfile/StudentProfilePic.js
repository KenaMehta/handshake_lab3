import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
import { getStudentQuery } from "./../../queries/queries";
import { updateStudentProfileMutation } from "./../../mutation/mutation";
import { graphql, compose } from "react-apollo";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import axios from "axios";
import configPath from "./../../configApp";
// import Dropzone from "react-dropzone";
// import request from "superagent";

class StudentProfilePic extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sid: localStorage.getItem("SID"),
      profileUpdateForm: "HideForm",
      updateTextValue: this.props.name,
      tempTextUpdate: "",
      profileObj: {},
      picture: "",
      profilePicture: ""
    };
  }

  updateProfilePic = () => {
    console.log(this.state);
    if (this.state.profileUpdateForm == "DisplayForm")
      this.setState({ profileUpdateForm: "HideForm" });
    else this.setState({ profileUpdateForm: "DisplayForm" });
  };

  handleUpdate = async e => {
    e.preventDefault();
    let mutationResponse = await this.props.updateStudentProfileMutation({
      variables: {
        name: this.state.tempTextUpdate,
        sid: localStorage.getItem("SID")
      },
      refetchQueries: [
        {
          query: getStudentQuery,
          variables: { sid: localStorage.getItem("SID") }
        }
      ]
    });
    this.updateProfilePic();
    // let response = mutationResponse.data.addMenuItem;
    // if (response) {
    //   if (response.status === "200") {
    //     this.setState({
    //       success: true,
    //       message: response.message
    //     });
    //   } else {
    //     this.setState({
    //       message: response.message
    //     });
    //   }
    // }
    // console.log("start coding handle update pro pic");
    // const data = { updateTextValue: this.state.tempTextUpdate };
    // this.props.updateStudentProfilePic(data);
    // let tempProfileObj = this.state.profileObj;
    // tempProfileObj.name = this.state.tempTextUpdate;
    // this.setState({
    //   profileObj: tempProfileObj
    // });
    // this.updateProfilePic();
  };

  getPage = () => {
    // axios
    //   .get(
    //     configPath.api_host + `/student/profile/${localStorage.getItem("SID")}`
    //   )
    //   .then(response => {
    //     if (response.status === 200) {
    //       console.log(
    //         JSON.stringify(response.data) + " : Axios Profile response"
    //       );
    var path = `${configPath.base}/images/` + `${this.props.PHOTO}`;
    // this.setState({ profileObj: response.data });
    this.setState({ profilePicture: path });
    // console.log(this.state.profileObj + " state after axios call");
    //   }
    // })
    // .catch(err => console.log(err + " : Error in getting Profile details"));
  };
  componentDidMount() {
    this.getPage();
  }
  //   componentWillRecieveProps(nextProps){
  // console.log(nextProps)
  //   }

  // updatePic = e => {
  //   e.preventDefault();
  //   let picdata = new FormData();
  //   picdata.append("myimage", this.state.picture);
  //   picdata.append("name", "profile");

  //   console.log("mounting in picture------------");
  //   console.log(this.state.picture);

  //   try {
  //     console.log("In try block");
  //     axios
  //       .post(
  //         configPath.api_host +
  //           `/student/profile/picture/${localStorage.getItem("SID")}`,
  //         picdata
  //       )
  //       .then(res => {
  //         console.log(res.data);
  //         var path = `${configPath.base}/images/` + `${res.data}`;
  //         console.log(path);
  //         this.setState({ profilePicture: path });
  //       })
  //       .catch(err => {
  //         console.log(err);
  //       });
  //   } catch (err) {
  //     console.log(err);
  //   }
  // };

  render() {
    return (
      <div align="center">
        <Card
          className="card_style"
          border="danger"
          bg="light"
          style={{ width: "18rem" }}
        >
          {/* {console.log(this.state.profilePicture.split("/")[4])} */}
          {/* {this.state.profilePicture.split("/")[4] != "null" ? ( */}
          <img
            align="center"
            style={{
              border: "1px solid #ddd",
              borderRadius: "4px",
              padding: "5px",
              width: "150px"
            }}
            src={`${configPath.base}/images/` + `${this.props.PHOTO}`}
          />
          {/* ) : (
            <form onSubmit={this.updatePic}>
              <button className="style__edit-photo___B-_os">
                <div>
                  <ion-icon
                    size="large"
                    name="camera"
                    style={{ color: "#1569e0" }}
                  />
                </div>

                <div>
                  {" "}
                  <input
                    style={{ color: "#1569e0", fontSize: "13px" }}
                    type="file"
                    name="file"
                    onChange={e => {
                      console.log(e.target.files[0]);
                      this.setState({ picture: e.target.files[0] });
                    }}
                  />
                </div>
              </button>
              <input
                style={{ fontSize: "10px" }}
                type="submit"
                className="btn btn-primary mt-3"
                value="Edit Pic"
              />
            </form>
          )} */}
          <Card.Body>
            <Card.Title>{this.props.name}</Card.Title>

            <Card.Text>{this.props.school}</Card.Text>
            <Card.Text>{this.props.MAJOR}</Card.Text>
            {this.props.DEGREE ? (
              <Card.Text style={{ color: "rgba(0, 0, 0, 0.56)" }}>
                {this.props.DEGREE} • GPA: {this.props.GPA}
              </Card.Text>
            ) : (
              <div />
            )}
            <Button
              className="btn-danger"
              onClick={this.updateProfilePic}
              variant="primary"
            >
              Update
            </Button>
          </Card.Body>
        </Card>
        <Card
          bg="light"
          style={{ width: "18rem" }}
          className={this.state.profileUpdateForm + " card_style edu-form"}
        >
          <Card.Body>
            <Form>
              <Form.Group controlId="formName">
                <Card.Title>Update Display Name</Card.Title>
                <input
                  onChange={e => {
                    this.setState({ tempTextUpdate: e.target.value });
                  }}
                  id="display_name"
                  style={{ margin: "20px" }}
                  type="text"
                  placeholder="Enter here"
                />
              </Form.Group>
              <Button
                className="btn-danger"
                onClick={
                  this.state.tempTextUpdate !== "" ? (
                    this.handleUpdate
                  ) : (
                    this.updateProfilePic
                  )
                }
                variant="primary"
              >
                Update
              </Button>
              <Button
                className="btn-secondary ml-3"
                onClick={() => {
                  this.setState({ profileUpdateForm: "HideForm" });
                }}
                variant="primary"
              >
                {" "}
                Cancel
              </Button>
            </Form>
          </Card.Body>
        </Card>
      </div>
    );
  }
}

// const mapStateToProps = state => {
//   console.log(
//     "inside profile pic update mapStateToProps " + state.studentProfileReducer
//   );
//   return {
//     name: state.studentProfileReducer.name
//       ? state.studentProfileReducer.name
//       : "",
//     school: state.studentProfileReducer.school
//       ? state.studentProfileReducer.school
//       : "",
//     DEGREE: state.studentProfileReducer.DEGREE
//       ? state.studentProfileReducer.DEGREE
//       : "",
//     MAJOR: state.studentProfileReducer.MAJOR
//       ? state.studentProfileReducer.MAJOR
//       : "",
//     GPA: state.studentProfileReducer.GPA ? state.studentProfileReducer.GPA : "",
//     profileUpdateFlag: state.studentProfileReducer.profileUpdateFlag
//       ? state.studentProfileReducer.profileUpdateFlag
//       : ""
//   };
// };

// const mapDispatchToProps = dispatch => {
//   console.log("calling thunk for propic");
//   return {
//     getStudentProfilePic: () => dispatch(getStudentProfilePic()),
//     updateStudentProfilePic: payload =>
//       dispatch(updateStudentProfilePic(payload))
//   };
// };

// export default StudentProfilePic;
export default compose(
  graphql(getStudentQuery, {
    options: {
      variables: { sid: localStorage.getItem("SID") }
    }
  }),
  graphql(updateStudentProfileMutation, {
    name: "updateStudentProfileMutation"
  })
)(StudentProfilePic);
// export default connect(mapStateToProps, mapDispatchToProps)(StudentProfilePic);
