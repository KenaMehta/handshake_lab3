import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
// import {
//   getStudentJourney,
//   updateStudentJourney
// } from "./../../actions/journeyAction";
import { getStudentQuery } from "./../../queries/queries";
import { updateStudentJourneyMutation } from "./../../mutation/mutation";
import { graphql, compose } from "react-apollo";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import axios from "axios";
import configPath from "./../../configApp";

class StudentJourney extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profileUpdateForm: "HideForm",
      journeyText: "",
      tempjourney: ""
    };
  }
  journeyUpdate = () => {
    if (this.state.profileUpdateForm == "DisplayForm")
      this.setState({ profileUpdateForm: "HideForm" });
    else this.setState({ profileUpdateForm: "DisplayForm" });
  };
  handleUpdate = async e => {
    e.preventDefault();
    let mutationResponse = await this.props.updateStudentJourneyMutation({
      variables: {
        journey: this.state.tempjourney,
        sid: localStorage.getItem("SID")
      },
      refetchQueries: [
        {
          query: getStudentQuery,
          variables: { sid: localStorage.getItem("SID") }
        }
      ]
    });
    this.journeyUpdate();

    // console.log("start coding handle update pro pic");
    // const data = { journey: this.state.tempjourney };
    // this.props.updateStudentJourney(data);
    // this.setState({
    //   journeyText: this.state.tempjourney
    // });
    // this.journeyUpdate();
  };
  // componentDidMount() {
  //   axios
  //     .get(
  //       configPath.api_host +
  //         `/student/profile/journey/${localStorage.getItem("SID")}`
  //     )
  //     .then(response => {
  //       if (response.status === 200) {
  //         console.log(
  //           JSON.stringify(response.data.journey) + " : Axios Journey response"
  //         );
  //         this.setState({ journeyText: response.data.journey });
  //         console.log(this.state.journeyText + " state after axios call");
  //       }
  //     })
  //     .catch(err => console.log(err + " : Error in getting Journey details"));
  // }
  render() {
    return (
      <div>
        <Card className="card_style" border="danger" bg="light" align="left">
          <Card.Body>
            <Card.Title>My Journey</Card.Title>
            <div className="style__divider___1j_Fp mb-3" />
            <Card.Text style={{ marginTop: "20px", marginBottom: "20px" }}>
              {this.props.journey}
            </Card.Text>

            <Button
              className="btn-danger"
              onClick={this.journeyUpdate}
              variant="primary"
            >
              Update
            </Button>
          </Card.Body>
        </Card>
        <Card
          bg="light"
          className={this.state.profileUpdateForm + " card_style edu-form"}
          align="center"
        >
          <Card.Body>
            <Card.Title>Update Your Journey</Card.Title>
            <textarea
              onChange={e => {
                this.setState({ tempjourney: e.target.value });
              }}
              style={{ marginTop: "20px", marginBottom: "20px" }}
              rows="4"
              type="textarea"
              placeholder="Enter here"
              className="form-control"
            />
            <Button
              className="btn-danger"
              onClick={
                //this.setState({journeyText:tempjourney})
                this.state.tempjourney !== "" ? (
                  this.handleUpdate
                ) : (
                  this.journeyUpdate
                )
              }
              variant="primary"
            >
              Update
            </Button>
            <Button
              className="btn-secondary ml-3"
              onClick={() => {
                this.setState({ profileUpdateForm: "HideForm" });
              }}
              variant="primary"
            >
              {" "}
              Cancel
            </Button>
          </Card.Body>
        </Card>
      </div>
    );
  }
}

// const mapStateToProps = state => {
//   console.log(
//     "inside journey update mapStateToProps " + state.studentProfileReducer
//   );
//   return {
//     journey: state.studentProfileReducer.journey
//   };
// };

// const mapDispatchToProps = dispatch => {
//   console.log("calling thunk for journey");
//   return {
//     getStudentJourney: () => dispatch(getStudentJourney()),
//     updateStudentJourney: payload => dispatch(updateStudentJourney(payload))
//   };
// };

// export default StudentJourney;
export default compose(
  graphql(getStudentQuery, {
    options: {
      variables: { sid: localStorage.getItem("SID") }
    }
  }),
  graphql(updateStudentJourneyMutation, {
    name: "updateStudentJourneyMutation"
  })
)(StudentJourney);
// export default connect(mapStateToProps, mapDispatchToProps)(StudentJourney);
