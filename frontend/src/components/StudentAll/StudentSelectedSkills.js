import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
import axios from "axios";
import configPath from "./../../configApp";
import { connect } from "react-redux";
import { Redirect } from "react-router";
class StudentSelectedSkills extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sid: localStorage.getItem("Selected_SID"),
      profileUpdateForm: "HideForm",
      deleteSkill: "",
      deleteFlag: false,
      skillArr: [],
      skill: ""
    };
  }
  updateSkills = () => {
    if (this.state.profileUpdateForm == "DisplayForm")
      this.setState({ profileUpdateForm: "HideForm" });
    else this.setState({ profileUpdateForm: "DisplayForm" });
  };
  // skillDelete = () => {
  //   console.log("code for delete");
  //   console.log(this.state.deleteSkill);
  //   let data = { deleteSkill: this.state.deleteSkill };
  //   axios
  //     .delete(
  //       configPath.api_host +
  //         `/student/profile/skills/${localStorage.getItem("SID")}`,
  //       { data: { data: data } }
  //     )
  //     .then(res => {
  //       console.log(
  //         JSON.stringify(res.data) + "---> skills obj after deleting"
  //       );
  //       this.setState({ deleteFlag: false });
  //       if (res.status === 200) {
  //         this.setState({ skillArr: res.data });
  //         console.log("Successfully Deleted");
  //       } else console.log("Error in deleting");
  //     })
  //     .catch(err => console.log("Error in catch: " + err));
  //   this.updateSkills();
  // };

  // skillAdd = () => {
  //   console.log("inside skills add");
  //   const dataAdd = {
  //     SKILL: this.state.skill
  //   };
  //   console.log(this.state.skill);
  //   axios
  //     .put(
  //       configPath.api_host +
  //         `/student/profile/skills/${localStorage.getItem("SID")}`,
  //       dataAdd
  //     )
  //     .then(res => {
  //       if (res.status === 200) {
  //         console.log(res.data);
  //         this.setState({
  //           skillArr: res.data
  //         });
  //       }
  //     })
  //     .catch(err => console.log(err + " err in adding/updating experience"));
  //   this.updateSkills();
  // };
  // componentDidMount() {
  //   console.log("In mount---------");
  //   axios
  //     .get(
  //       configPath.api_host +
  //         `/student/profile/skills/${localStorage.getItem("SID")}`
  //     )
  //     .then(response => {
  //       if (response.status === 200) {
  //         console.log(
  //           JSON.stringify(response.data) + " : Axios skills response"
  //         );
  //         this.setState({ skillArr: response.data });
  //         console.log(this.state.skillArr[0].SKILL + " state after axios call");
  //       }
  //     })
  //     .catch(err => console.log(err + " : Error in getting skills details"));
  // }
  render() {
    return (
      <div>
        <Card
          className="card_style"
          border="danger"
          bg="light"
          style={{ width: "18rem" }}
        >
          <Card.Body>
            <Card.Title>Your Skills</Card.Title>
            <div className="style__divider___1j_Fp mb-2" />
            <div className=" d-flex">
              {this.props.skillArr.map(skill => (
                <div class="student-skills">
                  <span class="style__tag___JUqHD" title="C++" data-hook="tag">
                    <span class="style__content___2INbm">
                      <span class="style__children___1bmK9">{skill.SKILL}</span>
                    </span>
                  </span>
                </div>
              ))}
            </div>
            {/* <Button
              className="btn-danger"
              variant="primary"
              onClick={e => {
                console.log("Add skill handler");
                this.updateSkills();
                this.setState({ deleteFlag: false });
              }}
            >
              Add
            </Button>
            <Button
              className="btn-danger"
              variant="primary"
              onClick={e => {
                try {
                  console.log("Delete skill handler");
                  this.updateSkills();
                  this.setState({ deleteFlag: true });
                  console.log(
                    JSON.stringify(this.state.deleteFlag) +
                      " state in delete handler"
                  );
                } catch (err) {
                  console.log(err);
                }
              }}
              style={{ marginLeft: "10px" }}
            >
              Delete
            </Button> */}
          </Card.Body>
        </Card>
        {this.state.deleteFlag ? (
          <Card
            bg="light"
            style={{ width: "18rem" }}
            className={this.state.profileUpdateForm + " card_style edu-form"}
          >
            <Card.Body>
              <Form>
                <Form.Group controlId="formName">
                  <Card.Title>Enter Skill Below</Card.Title>
                  <input
                    style={{ margin: "20px" }}
                    type="text"
                    placeholder="eg. Python/React/etc."
                    onChange={e => {
                      this.setState({ deleteSkill: e.target.value });
                    }}
                  />
                </Form.Group>
                <Button
                  className="btn-danger"
                  onClick={
                    this.state.deleteSkill !== "" ? (
                      this.skillDelete
                    ) : (
                      this.updateSkills
                    )
                  }
                  variant="primary"
                >
                  Delete
                </Button>
                <Button
                  className="btn-secondary ml-3"
                  onClick={() => {
                    this.setState({ profileUpdateForm: "HideForm" });
                  }}
                  variant="primary"
                >
                  {" "}
                  Cancel
                </Button>
              </Form>
            </Card.Body>
          </Card>
        ) : (
          <Card
            bg="light"
            style={{ width: "18rem" }}
            className={this.state.profileUpdateForm + " card_style edu-form"}
          >
            <Card.Body>
              <Form>
                <Form.Group controlId="formName">
                  <Card.Title>Enter Skill Below</Card.Title>
                  <input
                    style={{ margin: "20px" }}
                    type="text"
                    onChange={e => {
                      this.setState({ skill: e.target.value });
                    }}
                    placeholder="eg. Python/React/etc."
                  />
                </Form.Group>
                <Button
                  className="btn-danger"
                  onClick={
                    this.state.skill !== "" ? this.skillAdd : this.updateSkills
                  }
                  variant="primary"
                >
                  Add
                </Button>
                <Button
                  className="btn-secondary ml-3"
                  onClick={() => {
                    this.setState({ profileUpdateForm: "HideForm" });
                  }}
                  variant="primary"
                >
                  {" "}
                  Cancel
                </Button>
              </Form>
            </Card.Body>
          </Card>
        )}
      </div>
    );
  }
}

export default StudentSelectedSkills;
