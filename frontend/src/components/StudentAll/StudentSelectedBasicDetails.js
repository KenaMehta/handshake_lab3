import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
import axios from "axios";
import configPath from "./../../configApp";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import { getStudentQuery } from "./../../queries/queries";
import { updateStudentBasicDetailsMutation } from "./../../mutation/mutation";
import { graphql, compose } from "react-apollo";

class StudentSelectedBasicDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profileUpdateForm: "HideForm",
      detailObj: {},
      dob: "",
      city: "",
      state: "",
      country: "",
      phone: "",
      email: ""
    };
  }
  basicDetailUpdate = () => {
    if (this.state.profileUpdateForm == "DisplayForm")
      this.setState({ profileUpdateForm: "HideForm" });
    else this.setState({ profileUpdateForm: "DisplayForm" });
  };

  basicDetailAdd = async e => {
    e.preventDefault();
    const dataAdd = {
      DOB: this.state.dob,
      CITY: this.state.city,
      STATE: this.state.state,
      COUNTRY: this.state.country,
      PHONE: this.state.phone,
      EMAIL: this.state.email
    };
    let mutationResponse = await this.props.updateStudentBasicDetailsMutation({
      variables: {
        ...dataAdd,
        sid: localStorage.getItem("Selected_SID")
      },
      refetchQueries: [
        {
          query: getStudentQuery,
          variables: { sid: localStorage.getItem("Selected_SID") }
        }
      ]
    });
    this.basicDetailUpdate();

    // console.log("inside education add");
    // const dataAdd = {
    //   DOB: this.state.dob,
    //   CITY: this.state.city,
    //   STATE: this.state.state,
    //   COUNTRY: this.state.country,
    //   PHONE: this.state.phone,
    //   EMAIL: this.state.email
    // };
    // axios
    //   .post(
    //     configPath.api_host +
    //       `/student/profile/basic_details/${localStorage.getItem("SID")}`,
    //     dataAdd
    //   )
    //   .then(res => {
    //     if (res.status === 200) {
    //       console.log(res.data);
    //       this.setState({
    //         detailObj: res.data
    //       });
    //     }
    //   })
    //   .catch(err => console.log(err + " err in adding/updating"));

    // this.basicDetailUpdate();
  };
  // getDetails = () => {
  //   axios
  //     .get(
  //       configPath.api_host +
  //         `/student/profile/basic_details/${localStorage.getItem("SID")}`
  //     )
  //     .then(response => {
  //       if (response.status === 200) {
  //         console.log(
  //           JSON.stringify(response.data) + " : Axios basic detaiil response"
  //         );
  //         this.setState({ detailObj: response.data });
  //         console.log(
  //           JSON.stringify(this.state.detailObj) + " state after axios call"
  //         );
  //       }
  //     })
  //     .catch(err => console.log(err + " : Error in getting basic details"));
  // };
  // componentDidMount() {
  //   this.getDetails();
  // }
  componentWillReceiveProps(nextProps) {
    console.log(nextProps);
  }
  render() {
    return (
      <div>
        <Card
          className="card_style"
          border="danger"
          bg="light"
          align="left"
          style={{ width: "18rem" }}
        >
          <Card.Body>
            <Card.Title>Basic Details</Card.Title>
            <div className="style__divider___1j_Fp mb-3" />
            <div style={{ marginTop: "20px" }}>
              <div
                style={{
                  fontSize: "15px",
                  lineHeight: "24px",
                  fontWeight: "400",
                  color: "rgba(0,0,0,.8)"
                }}
              >
                <b>DOB:</b> {this.props.detailObj.dob}
              </div>
              <div
                style={{
                  fontSize: "16px",
                  lineHeight: "24px",
                  color: "rgba(0,0,0,.8)"
                }}
              >
                <b>CITY:</b> {this.props.detailObj.city}
              </div>
              <div
                style={{
                  fontSize: "15px",
                  lineHeight: "24px",
                  fontWeight: "400",
                  color: "rgba(0,0,0,.8)"
                }}
              >
                <b>STATE:</b> {this.props.detailObj.state}
              </div>
              <div
                style={{
                  fontSize: "15px",
                  lineHeight: "24px",
                  fontWeight: "400",
                  color: "rgba(0,0,0,.8)"
                }}
              >
                <b>COUNTRY:</b> {this.props.detailObj.country}
              </div>
              <div
                style={{
                  fontSize: "15px",
                  lineHeight: "24px",
                  color: "rgba(0,0,0,.8)"
                }}
              >
                <b>PHONE:</b> {this.props.detailObj.phone}
              </div>
              <div
                style={{
                  fontSize: "15px",
                  lineHeight: "24px",
                  color: "rgba(0,0,0,.8)"
                }}
              >
                <b>EMAIL:</b> {this.props.detailObj.email}
              </div>
            </div>

            {/* <Button
              className="btn-danger"
              style={{ marginTop: "20px", marginBottom: "10px" }}
              onClick={e => {
                this.basicDetailUpdate();
                this.setState({ deleteFlag: false });
              }}
              variant="primary"
            >
              Add/Update
            </Button> */}
          </Card.Body>
        </Card>

        <Card
          bg="light"
          className={this.state.profileUpdateForm + " card_style edu-form"}
          align="left"
        >
          <Card.Body>
            <Card.Title>Add/Update Basic Details</Card.Title>
            <div
              style={{
                fontSize: "13px",
                lineHeight: "20px",
                color: "rgba(0,0,0,.8)"
              }}
            />
            <div className="content margin-top" style={{ margin: "20px" }}>
              <form>
                <div className="form-group">
                  <label style={{ fontWeight: "bold" }}>DOB</label>
                  <input
                    name="date"
                    onChange={e => {
                      this.setState({ dob: e.target.value });
                    }}
                    type="date"
                    className="form-control"
                  />
                </div>
                <div className="form-group">
                  <label style={{ fontWeight: "bold" }}>City</label>
                  <input
                    name="city"
                    onChange={e => this.setState({ city: e.target.value })}
                    type="text"
                    placeholder="Enter City"
                    className="form-control"
                  />
                </div>
                <div className="form-group">
                  <label style={{ fontWeight: "bold" }}>State</label>
                  <input
                    name="state"
                    onChange={e => this.setState({ state: e.target.value })}
                    type="text"
                    placeholder="Enter State"
                    className="form-control"
                  />
                </div>

                <div className="form-group">
                  <label style={{ fontWeight: "bold" }}>Country</label>
                  <input
                    name="country"
                    onChange={e => this.setState({ country: e.target.value })}
                    type="text"
                    placeholder="Enter Country"
                    className="form-control"
                  />
                </div>
                <div className="form-group">
                  <label style={{ fontWeight: "bold" }}>Phone</label>
                  <input
                    name="phone"
                    onChange={e => this.setState({ phone: e.target.value })}
                    type="number"
                    maxLength="10"
                    minLength="10"
                    placeholder="Enter 10 digit number"
                    className="form-control"
                  />
                </div>

                <div />
              </form>
              <Button
                className="btn-danger"
                onClick={this.basicDetailAdd}
                variant="primary"
              >
                {" "}
                Add
              </Button>

              <Button
                className="btn-secondary ml-3"
                onClick={() => {
                  this.setState({ profileUpdateForm: "HideForm" });
                }}
                variant="primary"
              >
                {" "}
                Cancel
              </Button>
            </div>
          </Card.Body>
        </Card>
      </div>
    );
  }
}

// export default StudentBasicDetails;
export default compose(
  graphql(getStudentQuery, {
    options: {
      variables: { sid: localStorage.getItem("Selected_SID") }
    }
  }),
  graphql(updateStudentBasicDetailsMutation, {
    name: "updateStudentBasicDetailsMutation"
  })
)(StudentSelectedBasicDetails);
