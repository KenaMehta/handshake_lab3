import React, { Component } from "react";
import StudentSelectedProfilePic from "./StudentSelectedProfilePic";
import StudentSelectedSkills from "./StudentSelectedSkills";
import StudentSelectedJourney from "./StudentSelectedJourney";
import StudentSelectedEducation from "./StudentSelectedEducation";
import StudentSelectedExperience from "./StudentSelectedExperience";
import StudentSelectedBasicDetails from "./StudentSelectedBasicDetails";
import cookie from "react-cookies";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import { graphql } from "react-apollo";
import { getStudentQuery } from "../../queries/queries";

import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";

class StudentSelectedProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    let studentProfilePic = "",
      studentSkills = "",
      studentBasicDetails = "",
      studentJourney = "",
      studentEducation = "",
      studentExperience = "";
    if (localStorage.getItem("loginFlag") != "true") {
      return <Redirect to="/login" />;
    }
    let results = this.props.getStudentQuery;
    if (results && results.student) {
      console.log(results.student);
      studentProfilePic = (
        <StudentSelectedProfilePic
          PHOTO={results.student.PHOTO}
          name={results.student.name}
          school={results.student.school}
          DEGREE={results.student.DEGREE}
          MAJOR={results.student.MAJOR}
          GPA={results.student.GPA}
        />
      );
      studentSkills = (
        <StudentSelectedSkills skillArr={results.student.Skill} />
      );
      studentBasicDetails = (
        <StudentSelectedBasicDetails detailObj={results.student} />
      );
      studentJourney = (
        <StudentSelectedJourney journey={results.student.journey} />
      );
      studentEducation = (
        <StudentSelectedEducation educationArr={results.student.Education} />
      );
      studentExperience = (
        <StudentSelectedExperience experienceArr={results.student.Experience} />
      );
    }
    return (
      <div>
        <Row align="center">
          <Col className="head_line" md={12}>
            <h2 style={{ opacity: 0.7 }}>Your Profile</h2>
          </Col>
        </Row>
        <Container align="center">
          <Row>
            <Col md={4}>
              {studentProfilePic}
              {studentSkills}
              {studentBasicDetails}
            </Col>
            <Col md={8}>
              {studentJourney}
              {studentEducation}
              {studentExperience}
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default graphql(getStudentQuery, {
  name: "getStudentQuery",
  options: () => {
    return {
      variables: {
        sid: localStorage.getItem("Selected_SID")
      }
    };
  }
})(StudentSelectedProfile);
