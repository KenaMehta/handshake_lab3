import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/all.css";
// import {
//   getStudentProfilePic,
//   updateStudentProfilePic
// } from "./../../actions/profilepicAction";
// import { connect } from "react-redux";
import cookie from "react-cookies";
import { Redirect } from "react-router";
import axios from "axios";
import configPath from "./../../configApp";
import { graphql, withApollo } from "react-apollo";
import { getAllStudentsQuery } from "../../queries/queries";
// let student_filter = "none";
// let major_filter = "none";
// let skill_filter = "none";

class StudentAll extends Component {
  constructor(props) {
    super(props);
    this.state = {
      student_filter: "none",
      major_filter: "none",
      skill_filter: "none",
      studentArr: [],
      redirect: false,
      profilePicture: ""
    };
  }

  setRedirect = () => {
    this.setState({
      redirect: true
    });
  };
  renderRedirect = () => {
    const redirectPath = "/student/selected_student/" + this.state.selectedSID;
    if (this.state.redirect) {
      return <Redirect to={redirectPath} />;
    }
  };

  async componentDidMount() {
    const { data } = await this.props.client.query({
      query: getAllStudentsQuery,
      variables: {
        student_filter: this.state.student_filter,
        major_filter: this.state.major_filter,
        skill_filter: this.state.skill_filter
      },
      fetchPolicy: "no-cache"
    });
    console.log(data.getAllStudents.studentArr);
    this.setState({studentArr:data.getAllStudents.studentArr})
  }

  callGetPage = async () => {
    // console.log(this.state.studentArr + "   studentArr");
    // this.getPage();
    const { data } = await this.props.client.query({
      query: getAllStudentsQuery,
      variables: {
        student_filter: this.state.student_filter,
        major_filter: this.state.major_filter,
        skill_filter: this.state.skill_filter
      },
      fetchPolicy: "no-cache"
    });
    console.log(data);
    this.setState({studentArr:data.getAllStudents.studentArr})
    // console.log(this.state.major_filter);
    // student_filter =
    //   this.state.student_filter !== ""
    //     ? this.state.student_filter
    //     : student_filter;
    // major_filter =
    //   this.state.major_filter !== "" ? this.state.major_filter : major_filter;
    // console.log(major_filter);
    // skill_filter =
    //   this.state.skill_filter !== "" ? this.state.skill_filter : skill_filter;
  };

  // componentDidMount() {
  //   console.log("In componentDidMount");
  //   this.getPage();
  // }

  render() {
    if (localStorage.getItem("loginFlag") != "true") {
      return <Redirect to="/login" />;
    }
    let returnObj = "";
    if (localStorage.getItem("loginFlag") != "true") {
      return <Redirect to="/login" />;
    }
    // let results = this.props.getAllStudentsQuery;
    // if (results && results.getAllStudents)
    return (
      <div>
        {this.renderRedirect()}
        <Row className="head_line p-2" align="center">
          <Col md={12}>
            <h4 style={{ opacity: 0.7 }}>Explore Students</h4>
          </Col>
        </Row>
        <Container>
          <Row>
            <Col md={3}>
              <Card
                bg="light"
                className="mt-3 d-flex"
                style={{ width: "auto", marginBottom: "20px" }}
              >
                <div className="p-2">
                  <h4 className="card-title p-2" align="center">
                    Filter Students
                  </h4>
                  <div className="style__divider___1j_Fp mb-3" />
                  <div
                    className="d-flex"
                    style={{ left: "-10px", position: "relative" }}
                  >
                    <div
                      className="m-2"
                      style={{
                        left: "30px",
                        position: "relative",
                        opcaity: "0.5"
                      }}
                    >
                      <ion-icon name="people" />
                    </div>
                    <input
                      className="mb-4 pl-4"
                      onChange={e => {
                        this.setState(
                          { student_filter: e.target.value || "none" },
                          () => {
                            this.callGetPage();
                          }
                        );
                      }}
                      id="display_name"
                      //style={{ margin: "20px" }}
                      type="text"
                      placeholder="Filter on student/college"
                    />
                  </div>
                  <div
                    className="d-flex"
                    style={{ left: "-10px", position: "relative" }}
                  >
                    <div
                      className="m-2"
                      style={{
                        left: "30px",
                        position: "relative",
                        opcaity: "0.5"
                      }}
                    >
                      <ion-icon name="school" />
                    </div>
                    <input
                      className="mb-4 pl-4"
                      onChange={e => {
                        this.setState(
                          { major_filter: e.target.value || "none" },
                          () => {
                            this.callGetPage();
                          }
                        );
                      }}
                      id="display_name"
                      //style={{ margin: "20px" }}
                      type="text"
                      placeholder="Filter on Major"
                    />
                  </div>
                  <div
                    className="d-flex"
                    style={{ left: "-10px", position: "relative" }}
                  >
                    <div
                      className="m-2"
                      style={{
                        left: "30px",
                        position: "relative",
                        opcaity: "0.5"
                      }}
                    >
                      <ion-icon name="aperture" />
                    </div>
                    <input
                      className="mb-4 pl-4"
                      onChange={e => {
                        this.setState(
                          { skill_filter: e.target.value || "none" },
                          () => {
                            this.callGetPage();
                          }
                        );
                      }}
                      id="display_name"
                      //style={{ margin: "20px" }}
                      type="text"
                      placeholder="Filter on skills"
                    />
                  </div>
                </div>
              </Card>
            </Col>
            <Col md={8}>
              {this.state.studentArr ? (
                <div>
                  <div>
                    {this.state.studentArr.map(student => (
                      <Card
                        bg="light"
                        className="e card_style m-3 p-2"
                        onClick={e => {
                          console.log(student.SID + " in div onclick");
                          localStorage.setItem("Selected_SID", student.SID);
                          this.setState({ selectedSID: student.SID }, () =>
                            this.setRedirect()
                          );
                        }}
                        style={{
                          width: "auto",
                          marginBottom: "20px",
                          height: "auto"
                        }}
                      >
                        <div className="d-flex">
                          {console.log(student)}
                          <div className="p-2 col-2">
                            {student.profilePic != null ? (
                              <img
                                style={{
                                  border: "1px solid #ddd",
                                  borderRadius: "4px",
                                  padding: "5px",
                                  width: "70px"
                                }}
                                src={
                                  `${configPath.base}/images/` +
                                  student.profilePic
                                }
                              />
                            ) : (
                              <div className="mt-3 ml-3">
                                <ion-icon size="large" name="person-outline" />
                              </div>
                            )}
                          </div>
                          <div className="p-2">
                            <div
                              style={{
                                fontWeight: "600",
                                fontSize: "18px"
                              }}
                            >
                              {student.name}
                            </div>
                            <div
                              style={{
                                fontWeight: "500",
                                fontSize: "14px"
                              }}
                            >
                              {student.school}
                            </div>
                            <div
                              style={{
                                fontWeight: "400",
                                fontSize: "13px"
                              }}
                            >
                              {student.DEGREE}, Year of Passing:{" "}
                              {student.YEAR_OF_PASSING} | {student.MAJOR}
                            </div>
                            <div
                              style={{
                                fontWeight: "400",
                                fontSize: "13px"
                              }}
                            >
                              {student.SkillList}
                            </div>
                          </div>
                        </div>
                      </Card>
                    ))}
                  </div>
                </div>
              ) : (
                <div />
              )}
            </Col>
          </Row>
        </Container>
      </div>
    );
    // else return <div />;
  }
}

export default withApollo(StudentAll);
// export default graphql(getAllStudentsQuery, {
//   name: "getAllStudentsQuery",
//   options: () => {
//     return {
//       variables: {
//         student_filter,
//         major_filter,
//         skill_filter
//       }
//     };
//   }
// })(StudentAll);
