import React, { Component } from "react";
import { Link } from "react-router-dom";
import cookie from "react-cookies";
import { Redirect } from "react-router";
import homePic from "./images/handshake_home.jpg";
import PropTypes from "prop-types";
import { connect } from "react-redux";
// import { logOut } from "./../../actions/loginAction";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Navbar,
  Nav,
  Image,
  FormControl
} from "react-bootstrap";

//create the Navbar Component
class Navigation extends Component {
  constructor(props) {
    super(props);
    this.handleLogout = this.handleLogout.bind(this);
  }
  //handle logout to destroy the cookie
  handleLogout = () => {
    console.log("inside handleLogout");
    localStorage.clear();
  };

  render() {
    var isActive =
      this.context.router.route.location.pathname === this.props.to;
    var className = isActive ? "active" : "";
    //if Cookie is set render Logout Button
    let navLogin = null;

    if (localStorage.getItem("loginFlag")) {
      console.log("Able to read cookie");
      navLogin = (
        <Navbar bg="dark" variant="dark">
          <Navbar.Brand style={{ color: "red", fontWeight: "bold" }} href="/">
            HandShake
          </Navbar.Brand>
          <Nav className="mr-auto">
            {localStorage.getItem("category") == "student" ? (
              <div className="d-flex">
                <Nav.Link href="/student/profile">Profile</Nav.Link>
                <Nav.Link href="/student/job_list">Jobs</Nav.Link>
                <Nav.Link href="/student/event_list">Events</Nav.Link>
                <Nav.Link href="/student/all_students">Students</Nav.Link>
                <Nav.Link href="/student/applications">Applications</Nav.Link>
              </div>
            ) : (
              <div className="d-flex">
                <Nav.Link href="/company/profile">Profile</Nav.Link>
                <Nav.Link href="/company/event_list">Events</Nav.Link>
                <Nav.Link href="/company/all_students">Students</Nav.Link>
              </div>
            )}

            <Nav.Link
              href="/login"
              onClick={this.handleLogout}
              style={{ fontWeight: "bold" }}
            >
              Logout
            </Nav.Link>
          </Nav>
          <Form inline>
            <FormControl type="text" placeholder="Search" className="mr-sm-2" />
            <Button className="btn btn-danger">Search</Button>
          </Form>
        </Navbar>
      );
    } else {
      //Else display login button
      console.log("No cookie");
      navLogin = (
        <div>
          <Navbar bg="dark" variant="dark">
            <Navbar.Brand style={{ color: "red", fontWeight: "bold" }} href="/">
              HandShake
            </Navbar.Brand>
            <Nav className="mr-auto">
              <Nav.Link href="/registerStudent">Student SignUp</Nav.Link>
              <Nav.Link href="/registerCompany">Company SignUp</Nav.Link>
              <Nav.Link href="/login" style={{ fontWeight: "bold" }}>
                LogIn
              </Nav.Link>
            </Nav>
          </Navbar>
        </div>
      );
    }

    return <div>{navLogin}</div>;
  }
}

Navigation.contextTypes = {
  router: PropTypes.object
};

// const mapStateToProps = state => {
//   return {
//     logoutFlag: state.loginReducer.logoutFlag
//   };
// };
// const mapDispatchToProps = dispatch => {
//   return {
//     logOut: payload => dispatch(logOut())
//   };
// };

//export Navigation Component
export default Navigation
