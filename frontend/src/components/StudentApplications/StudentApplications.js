import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/applications.css";
import { graphql, withApollo, compose } from "react-apollo";
import { getStudentApplicationsQuery } from "../../queries/queries";
// import cookie from "react-cookies";
import { Redirect } from "react-router";
import axios from "axios";
import configPath from "./../../configApp";

class StudentApplications extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pending_filter: "false",
      reviewed_filter: "false",
      declined_filter: "false",
      jobListArr: []
    };
  }

  // getPage = () => {
  //   console.log(this.state.pending_filter);
  //   axios
  //     .get(
  //       configPath.api_host +
  //         `/student/applications/${localStorage.getItem("SID")}/${this.state
  //           .pending_filter}/${this.state.reviewed_filter}/${this.state
  //           .declined_filter}`
  //     )
  //     .then(response => {
  //       if (response.status === 200) {
  //         this.setState({ jobListArr: response.data });
  //       }
  //     })
  //     .catch(err => console.log(err + " : Error in getting Job details"));
  // };
  callGetPage = async () => {
    const { data } = await this.props.client.query({
      query: getStudentApplicationsQuery,
      variables: {
        pending_filter: this.state.pending_filter,
        reviewed_filter: this.state.reviewed_filter,
        declined_filter: this.state.declined_filter,
        sid: localStorage.getItem("SID")
      },
      fetchPolicy: "no-cache"
    });
    console.log(data);
    this.setState({
      jobListArr: data.getStudentApplications.jobListArr
    });

    // console.log(this.state + "filter");
    // this.getPage();
  };

  async componentDidMount() {
    const { data } = await this.props.client.query({
      query: getStudentApplicationsQuery,
      variables: {
        pending_filter: this.state.pending_filter,
        reviewed_filter: this.state.reviewed_filter,
        declined_filter: this.state.declined_filter,
        sid: localStorage.getItem("SID")
      },
      fetchPolicy: "no-cache"
    });
    console.log(data);
    this.setState({
      jobListArr: data.getStudentApplications.jobListArr
    });

    // console.log("In componentDidMount");
    // this.getPage();
  }

  render() {
    // if (!cookie.load("cookie")) {
    //   return <Redirect to="/login" />;
    // }
    if (localStorage.getItem("loginFlag") != "true") {
      return <Redirect to="/login" />;
    }
    return (
      <div>
        <Row align="center">
          <Col className="head_line" md={12}>
            <h2 style={{ opacity: 0.7 }}>Your Applications</h2>
          </Col>
        </Row>
        <Container>
          <Row>
            <Col md={3}>
              <Card
                bg="light"
                className="card_style d-flex"
                style={{ width: "auto", marginBottom: "20px", height: "auto" }}
              >
                <div className="style__jobs___3seWY p-2">
                  <h4 className="card-title mb-3" align="center">
                    Filters
                  </h4>
                  <div>
                    <div className="style__divider___1j_Fp mb-2" />
                    <label>
                      <input
                        id="status"
                        name="status"
                        className="mr-2"
                        type="radio"
                        value="status"
                        onClick={() =>
                          this.setState(
                            {
                              reviewed_filter: "false",
                              pending_filter: "true",
                              declined_filter: "false"
                            },
                            () => this.callGetPage()
                          )}
                      />Pending
                    </label>
                  </div>
                  <div>
                    <label>
                      <input
                        id="status"
                        name="status"
                        className="mr-2"
                        type="radio"
                        value="status"
                        onClick={() =>
                          this.setState(
                            {
                              reviewed_filter: "true",
                              pending_filter: "false",
                              declined_filter: "false"
                            },
                            () => this.callGetPage()
                          )}
                      />Reviewed
                    </label>
                  </div>

                  <div>
                    <label>
                      <input
                        id="status"
                        name="status"
                        className="mr-2"
                        type="radio"
                        value="status"
                        onClick={() =>
                          this.setState(
                            {
                              reviewed_filter: "false",
                              pending_filter: "false",
                              declined_filter: "true"
                            },
                            () => this.callGetPage()
                          )}
                      />Declined
                    </label>
                  </div>
                  <div>
                    <input
                      className="mr-2"
                      type="button"
                      value="Clear"
                      onClick={() =>
                        this.setState(
                          {
                            reviewed_filter: "false",
                            pending_filter: "false",
                            declined_filter: "false"
                          },
                          () => {
                            var tick = document.getElementsByName("status");
                            for (var i = 0; i < tick.length; i++)
                              tick[i].checked = false;
                            this.callGetPage();
                          }
                        )}
                    />
                  </div>
                </div>
              </Card>
            </Col>
            <Col md={8}>
              {this.state.jobListArr ? (
                <div>
                  <div>
                    {this.state.jobListArr.map(job => (
                      <Card
                        bg="light"
                        className="card_style m-3 p-2"
                        style={{
                          width: "auto",
                          marginBottom: "20px",
                          height: "auto"
                        }}
                      >
                        <div
                          style={{
                            fontWeight: "600",
                            fontSize: "18px"
                          }}
                        >
                          {job.JID.JOB_TITLE}
                        </div>
                        <div
                          style={{
                            fontWeight: "500",
                            fontSize: "14px"
                          }}
                        >
                          {job.JID.C_NAME}
                        </div>
                        <div
                          className="d-flex"
                          style={{
                            fontSize: "14px",
                            color: "rgba(0,0,0,.56)"
                          }}
                        >
                          <div
                            style={{
                              top: "2px",
                              position: "relative",
                              opacity: "0.7"
                            }}
                          >
                            <ion-icon name="information" />
                          </div>
                          status: {job.STATUS}
                        </div>
                        <div
                          className="d-flex"
                          style={{
                            fontSize: "14px",
                            color: "rgba(0,0,0,.56)"
                          }}
                        >
                          <div
                            style={{
                              top: "2px",
                              position: "relative",
                              opacity: "0.7"
                            }}
                          >
                            <ion-icon name="checkmark" />
                          </div>
                          Applied {job.CreatedAt} - Applications closes on{" "}
                          {job.JID.APP_DEADLINE}
                        </div>
                      </Card>
                    ))}
                  </div>
                </div>
              ) : (
                <div />
              )}
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

// export default StudentApplications;
export default withApollo(StudentApplications);
