import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
import "../styles/job.css";
import cookie from "react-cookies";
import { Redirect } from "react-router";
import axios from "axios";
import configPath from "./../../configApp";
import { graphql, withApollo, compose } from "react-apollo";
import {
  getCompanyJobsQuery,
  getSelectedJobQuery
} from "../../queries/queries";
import { applyJobMutation } from "../../mutation/mutation";
let job_id = "";

class StudentJobs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      category_filter: "none",
      company_filter: "none",
      job_filter: "none",
      location_filter: "none",
      jobArr: [],
      jobObj: {},
      modalBox: "HideBox",
      application_status: "",
      resume: "",
      selectedCompany: "",
      redirect: false
    };
  }
  modalUpdate = () => {
    this.setState({ application_status: "" });
    if (this.state.modalBox == "DisplayBox")
      this.setState({ modalBox: "HideBox" });
    else this.setState({ modalBox: "DisplayBox" });
  };
  displayJob = async job_id => {
    const { data } = await this.props.client.query({
      query: getSelectedJobQuery,
      variables: { job_id },
      fetchPolicy: "no-cache"
    });
    console.log(data.getSelectedJob);
    this.setState({
      jobObj: data.getSelectedJob
    });
    // console.log("in getPage");
    // axios
    //   .post(configPath.api_host + `/student/job_list`, { job_id: job_id })
    //   .then(response => {
    //     if (response.status === 200) {
    //       //   console.log(
    //       //     JSON.stringify(response.data) + " : Axios Profile response"
    //       //   );
    //       this.setState({ jobObj: response.data });
    //       //   console.log(
    //       //     JSON.stringify(this.state.jobArr) + " state after axios call"
    //       //   );
    //     }
    //   })
    //   .catch(err => console.log(err + " : Error in getting Profile details"));
  };

  // getPage = () => {
  //   console.log(this.state.company_filter);
  //   axios
  //     .get(
  //       configPath.api_host +
  //         `/student/job_list/${this.state.category_filter}/${this.state
  //           .company_filter}/${this.state.job_filter}/${this.state
  //           .location_filter}`
  //     )
  //     .then(response => {
  //       if (response.status === 200) {
  //         this.setState({ jobArr: response.data });
  //         this.setState({ jobObj: response.data[0] });
  //       }
  //     })
  //     .catch(err => console.log(err + " : Error in getting Profile details"));
  // };

  // async componentWillReceiveProps(nextProps) {
  //   const results = await nextProps.getCompanyJobsQuery;
  //   if (results && results.getCompanyJobs) {
  //     console.log(results.getCompanyJobs);
  //     this.setState({
  //       jobArr: results.getCompanyJobs.jobArr,
  //       jobObj: results.getCompanyJobs.jobObj
  //     });
  //   }}
  async componentDidMount() {
    const { data } = await this.props.client.query({
      query: getCompanyJobsQuery,
      variables: {
        category_filter: this.state.category_filter,
        company_filter: this.state.company_filter,
        job_filter: this.state.job_filter,
        location_filter: this.state.location_filter
      },
      fetchPolicy: "no-cache"
    });
    console.log(data.getCompanyJobs.jobArr);
    this.setState({
      jobArr: data.getCompanyJobs.jobArr,
      jobObj: data.getCompanyJobs.jobObj
    });
  }

  callGetPage = async () => {
    const { data } = await this.props.client.query({
      query: getCompanyJobsQuery,
      variables: {
        category_filter: this.state.category_filter,
        company_filter: this.state.company_filter,
        job_filter: this.state.job_filter,
        location_filter: this.state.location_filter
      },
      fetchPolicy: "no-cache"
    });
    console.log(data.getCompanyJobs.jobArr);
    this.setState({
      jobArr: data.getCompanyJobs.jobArr,
      jobObj: data.getCompanyJobs.jobObj
    });
  };

  setRedirect = () => {
    this.setState({
      redirect: true
    });
  };
  renderRedirect = () => {
    const redirectPath =
      "/company/selected_company/" + this.state.selectedCID;
    if (this.state.redirect) {
      return <Redirect to={redirectPath} />;
    }
  };
  studentApply = async e => {
    e.preventDefault();

    let mutationResponse = await this.props.applyJobMutation({
      variables: {
        JOB_CODE: this.state.jobObj._id,
        SID: localStorage.getItem("SID")
      }
    });
    let results = mutationResponse.data.applyJob;
    this.setState({ message: results.message });
    console.log(mutationResponse);
    // console.log("In studentApply");
    // const data = new FormData();
    // console.log(this.state.selectedFile);
    // data.append("resume", this.state.selectedFile);
    // data.append("SID", localStorage.getItem("SID"));
    // data.append("JOB_CODE", this.state.jobObj.C_J_PK);
    // axios
    //   .post(configPath.api_host + `/student/job_list/job_apply`, data)
    //   .then(response => {
    //     console.log(JSON.stringify(response.data) + "iiin stu app");
    //     this.setState({ application_status: response.data.application_status });
    //     var path = `${configPath.base}/images/` + `${response.data.resume}`;
    //     this.setState({ resume: path });
    //   })
    //   .catch(error => {
    //     console.log(error);
    //     this.setState({
    //       application_status: error.response.data.application_status
    //     });
    //   });
  };
  // componentDidMount() {
  //   console.log("In componentDidMount");
  //   this.getPage();
  // }

  render() {
    // if (!cookie.load("cookie")) {
    //   return <Redirect to="/login" />;
    // }
    if (localStorage.getItem("loginFlag") != "true") {
      return <Redirect to="/login" />;
    }
    return (
      <div>
        {this.renderRedirect()}
        <Row align="center">
          <Col className="head_line" md={12}>
            <h2 style={{ opacity: 0.7 }}>Jobs</h2>
          </Col>
        </Row>
        <Container>
          <Row>
            <Col md={12}>
              <Card
                border="danger"
                bg="light"
                className="card_style d-flex m-3"
                style={{ width: "auto" }}
              >
                <div className="d-flex p-2">
                  <div
                    className="m-2"
                    style={{ left: "30px", top: "19px", position: "relative" }}
                  >
                    <ion-icon name="podium-outline" />
                  </div>
                  <input
                    className="form-control mt-3 pl-4"
                    onChange={e => {
                      this.setState(
                        { company_filter: e.target.value || "none" },
                        () => {
                          this.callGetPage();
                        }
                      );
                    }}
                    id="display_name"
                    //style={{ margin: "20px" }}
                    type="text"
                    placeholder="Filter on company"
                  />
                  <div
                    className="m-2"
                    style={{ left: "30px", top: "19px", position: "relative" }}
                  >
                    <ion-icon name="briefcase-outline" />
                  </div>
                  <input
                    className="form-control mt-3 pl-4"
                    onChange={e => {
                      this.setState(
                        { job_filter: e.target.value || "none" },
                        () => {
                          this.callGetPage();
                        }
                      );
                    }}
                    id="display_name"
                    //style={{ margin: "20px" }}
                    type="text"
                    placeholder="Filter on job title"
                  />
                  <div
                    className="m-2"
                    style={{ left: "30px", top: "19px", position: "relative" }}
                  >
                    <ion-icon name="location-outline" />
                  </div>
                  <input
                    className="form-control mt-3 pl-4"
                    onChange={e => {
                      this.setState(
                        { location_filter: e.target.value || "none" },
                        () => {
                          this.callGetPage();
                        }
                      );
                    }}
                    id="display_name"
                    //style={{ margin: "20px" }}
                    type="text"
                    placeholder="Filter on city"
                  />
                </div>
                <div className="d-flex m-3 p-2">
                  <button
                    style={{ borderRadius: "20px" }}
                    type="button"
                    className="btn btn-outline-secondary m-2"
                    onClick={e => {
                      console.log("on-campus button");
                      this.setState({ category_filter: "on-campus" }, () => {
                        console.log("Changing state");
                        this.callGetPage();
                      });
                    }}
                  >
                    On-Campus
                  </button>
                  <button
                    style={{ borderRadius: "20px" }}
                    className="btn btn-outline-secondary m-2"
                    onClick={e => {
                      console.log("full-time button");
                      this.setState({ category_filter: "full-time" }, () => {
                        console.log("Changing state");
                        this.callGetPage();
                      });
                    }}
                  >
                    Full Time
                  </button>
                  <button
                    style={{ borderRadius: "20px" }}
                    className="btn btn-outline-secondary m-2"
                    onClick={e => {
                      console.log("parttime button");
                      this.setState({ category_filter: "part-time" }, () => {
                        console.log("Changing state");
                        this.callGetPage();
                      });
                    }}
                  >
                    Part Time
                  </button>
                  <button
                    style={{ borderRadius: "20px" }}
                    className="btn btn-outline-secondary m-2"
                    onClick={e => {
                      console.log("internship button");
                      this.setState({ category_filter: "internship" }, () => {
                        console.log(this.state.category_filter);
                        this.callGetPage();
                      });
                    }}
                  >
                    Internship
                  </button>
                </div>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col md={4}>
              <Card
                bg="light"
                className="card_style d-flex"
                style={{ width: "auto", marginBottom: "20px", height: "500px" }}
              >
                <div className="style__jobs___3seWY p-2">
                  <h4 className="card-title" align="center">
                    List of Jobs
                  </h4>
                  <div>
                    {this.state.jobArr.map(job => (
                      <div
                        onClick={e => {
                          console.log(job._id + " in div onclick");
                          this.displayJob(job._id);
                        }}
                        className="e mb-3"
                      >
                        <div className="d-flex">
                          <div className="p-2 col-3">
                            {job.CID.PROFILE_PIC != null ? (
                              <img
                                style={{
                                  border: "1px solid #ddd",
                                  borderRadius: "4px",
                                  padding: "5px",
                                  width: "50px"
                                }}
                                src={
                                  `${configPath.base}/images/` +
                                  job.CID.PROFILE_PIC
                                }
                              />
                            ) : (
                              <div className="mt-3 ml-3">
                                <ion-icon size="large" name="person-outline" />
                              </div>
                            )}
                          </div>
                          <div className="p-2">
                            <div
                              style={{
                                fontWeight: "bold",
                                fontSize: "13px"
                              }}
                            >
                              {job.JOB_TITLE}
                            </div>
                            <div
                              style={{
                                fontSize: "12px"
                              }}
                            >
                              {job.C_NAME} - {job.CITY}, {job.STATE}
                            </div>
                            <div
                              style={{
                                fontSize: "12px",
                                color: "rgba(0,0,0,.56)"
                              }}
                            >
                              {job.JOB_CATEGORY} job
                            </div>
                          </div>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
              </Card>
            </Col>
            <Col md={8}>
              {this.state.jobObj ? (
                <div>
                  <Card
                    bg="light"
                    className="card_style d-flex m-3 p-2"
                    style={{
                      width: "auto",
                      marginBottom: "20px",
                      height: "500px"
                    }}
                  >
                    <div className="style__jobs___3seWY p-2">
                      <div className="d-flex">
                        <div className="p-2 col-2">
                          {this.state.jobObj.CID ? this.state.jobObj.CID
                            .PROFILE_PIC != null ? (
                            <img
                              style={{
                                border: "1px solid #ddd",
                                borderRadius: "4px",
                                padding: "5px",
                                width: "70px"
                              }}
                              src={
                                `${configPath.base}/images/` +
                                this.state.jobObj.CID.PROFILE_PIC
                              }
                            />
                          ) : (
                            <div className="mt-3 ml-3">
                              <ion-icon size="large" name="person-outline" />
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div>
                          <div style={{ fontSize: "24px", fontWeight: "500" }}>
                            {this.state.jobObj.JOB_TITLE}
                          </div>
                          <div
                            className="e_c"
                            style={{ fontSize: "16px", fontWeight: "400" }}
                            onClick={e => {
                              console.log(
                                this.state.jobObj.CID._id + " in div onclick"
                              );
                              localStorage.setItem(
                                "Selected_CID",
                                this.state.jobObj.CID._id
                              );
                              localStorage.setItem(
                                "Selected_Company",
                                this.state.jobObj.C_NAME
                              );
                              this.setState(
                                { selectedCID: this.state.jobObj.CID._id },
                                () => this.setRedirect()
                              );
                            }}
                          >
                            {this.state.jobObj.C_NAME}
                          </div>
                          <div className="d-flex">
                            <div>
                              <ion-icon
                                style={{
                                  opacity: "0.6"
                                }}
                                name="briefcase-outline"
                              />
                            </div>
                            <div
                              style={{
                                left: "15px",
                                fontSize: "14px",
                                color: "rgba(0,0,0,.56)"
                              }}
                            >
                              {this.state.jobObj.JOB_CATEGORY} job •{"  "}
                            </div>
                            <div>
                              <ion-icon
                                style={{
                                  opacity: "0.6"
                                }}
                                name="location-outline"
                              />
                            </div>
                            <div
                              style={{
                                left: "15px",
                                fontSize: "14px",
                                color: "rgba(0,0,0,.56)"
                              }}
                            >
                              {this.state.jobObj.CITY},{" "}
                              {this.state.jobObj.STATE} •{"  "}
                            </div>
                            <div>
                              <ion-icon
                                style={{
                                  opacity: "0.6"
                                }}
                                name="cash-outline"
                              />
                            </div>
                            <div
                              style={{
                                left: "15px",
                                fontSize: "14px",
                                color: "rgba(0,0,0,.56)"
                              }}
                            >
                              {this.state.jobObj.SALARY} •{"  "}
                            </div>
                            <div>
                              <ion-icon
                                style={{
                                  opacity: "0.6"
                                }}
                                name="time-outline"
                              />
                            </div>
                            <div
                              style={{
                                left: "15px",
                                fontSize: "14px",
                                color: "rgba(0,0,0,.56)"
                              }}
                            >
                              Posted on{" "}
                              {this.state.jobObj.Created_At ? (
                                this.state.jobObj.Created_At.substring(0, 16)
                              ) : (
                                ""
                              )}
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="style__card___31yrn mt-2 d-flex justify-content-between">
                        <div className="style__large___3qwwG mt-2">
                          Applications close on{" "}
                          {this.state.jobObj.APP_DEADLINE ? (
                            this.state.jobObj.APP_DEADLINE.substring(0, 16)
                          ) : (
                            ""
                          )}
                        </div>

                        <button
                          onClick={this.modalUpdate}
                          type="button"
                          className="btn btn-outline-danger style__base___hEhR9"
                        >
                          <span>Apply</span>
                        </button>
                      </div>
                      <div>{this.state.jobObj.JOB_DESC}</div>
                    </div>
                  </Card>
                  <Card className={this.state.modalBox + " modal"}>
                    <div className="modal-content">
                      <Container>
                        <span
                          className="close"
                          onClick={() => {
                            this.modalUpdate();
                            this.setState({
                              message: ""
                            });
                          }}
                        >
                          &times;
                        </span>
                        <div>
                          <h2 className="style__heading___29i1Z">
                            <span>Apply to {this.state.jobObj.C_NAME}</span>
                          </h2>
                        </div>
                        <div>
                          <h3 className="style__heading___29i1Z style__medium___m_Ip7">
                            Details from {this.state.jobObj.C_NAME}:
                          </h3>
                        </div>
                        <div className="style__text___2ilXR">
                          Applying for {this.state.jobObj.JOB_TITLE} requires a
                          few documents. Attach them below and get one step
                          closer to your next job!
                        </div>
                        <form className="mt-3" onSubmit={this.studentApply}>
                          <label>Upload your resume below:</label>
                          <div className="d-flex">
                            <div className="m-3">
                              <input
                                type="file"
                                name="resume"
                                id="resume"
                                onChange={event => {
                                  console.log(event.target.files[0]);
                                  this.setState({
                                    selectedFile: event.target.files[0]
                                  });
                                }}
                              />
                            </div>
                            <div className="mt-2">
                              {this.state.resume ? (
                                <a
                                  href={this.state.resume}
                                  download="Resume"
                                  target="_blank"
                                >
                                  Download
                                </a>
                              ) : (
                                ""
                              )}
                            </div>
                          </div>
                          <div className="m-3">
                            <input type="submit" value="Submit" />

                            <span style={{ fontWeight: "bold", color: "red" }}>
                              {this.state.message}
                            </span>
                          </div>
                        </form>
                      </Container>
                    </div>
                  </Card>
                </div>
              ) : (
                <div />
              )}
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

// export default withApollo(StudentJobs);

export default compose(
  withApollo,
  graphql(applyJobMutation, { name: "applyJobMutation" })
)(StudentJobs);
