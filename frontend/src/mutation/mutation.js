import { gql } from "apollo-boost";

export const loginMutation = gql`
  mutation login($category: String, $email_id: String, $password: String) {
    login(category: $category, email_id: $email_id, password: $password) {
      status
      message
    }
  }
`;

export const registerStudentMutation = gql`
  mutation registerStudent(
    $name: String
    $email: String
    $password1: String
    $college_name: String
  ) {
    registerStudent(
      name: $name
      email: $email
      password1: $password1
      college_name: $college_name
    ) {
      status
      message
    }
  }
`;

export const registerCompanyMutation = gql`
  mutation registerCompany(
    $name: String
    $email: String
    $password1: String
    $location: String
  ) {
    registerCompany(
      name: $name
      email: $email
      password1: $password1
      location: $location
    ) {
      status
      message
    }
  }
`;

export const updateStudentProfileMutation = gql`
  mutation updateStudentProfile($name: String, $sid: String) {
    updateStudentProfile(name: $name, sid: $sid) {
      status
      message
    }
  }
`;
export const updateStudentBasicDetailsMutation = gql`
  mutation updateStudentBasicDetails(
    $DOB: String
    $CITY: String
    $STATE: String
    $COUNTRY: String
    $PHONE: String
    $EMAIL: String
    $sid: String
  ) {
    updateStudentBasicDetails(
      DOB: $DOB
      CITY: $CITY
      STATE: $STATE
      COUNTRY: $COUNTRY
      PHONE: $PHONE
      EMAIL: $EMAIL
      sid: $sid
    ) {
      status
      message
    }
  }
`;

export const updateStudentJourneyMutation = gql`
  mutation updateStudentJourney($journey: String, $sid: String) {
    updateStudentJourney(journey: $journey, sid: $sid) {
      status
      message
    }
  }
`;

export const updateStudentEducationMutation = gql`
  mutation updateStudentEducation(
    $COLLEGE_NAME: String
    $DEGREE: String
    $MAJOR: String
    $YEAR_OF_PASSING: String
    $CURRENT_GPA: String
    $sid: String
  ) {
    updateStudentEducation(
      COLLEGE_NAME: $COLLEGE_NAME
      DEGREE: $DEGREE
      MAJOR: $MAJOR
      YEAR_OF_PASSING: $YEAR_OF_PASSING
      CURRENT_GPA: $CURRENT_GPA
      sid: $sid
    ) {
      status
      message
    }
  }
`;

export const updateStudentExperienceMutation = gql`
  mutation updateStudentExperience(
    $COMPANY_NAME: String
    $TITLE: String
    $LOCATION: String
    $START_DT: String
    $END_DT: String
    $WORK_DESC: String
    $sid: String
  ) {
    updateStudentExperience(
      COMPANY_NAME: $COMPANY_NAME
      TITLE: $TITLE
      LOCATION: $LOCATION
      START_DT: $START_DT
      END_DT: $END_DT
      WORK_DESC: $WORK_DESC
      sid: $sid
    ) {
      status
      message
    }
  }
`;

export const applyJobMutation = gql`
  mutation applyJob($JOB_CODE: String, $SID: String) {
    applyJob(JOB_CODE: $JOB_CODE, SID: $SID) {
      status
      message
    }
  }
`;

export const updateCompanyProfileMutation = gql`
  mutation updateCompany(
    $NAME: String
    $LOCATION: String
    $DESCRIPTION: String
    $PHONE: String
    $cid: String
  ) {
    updateCompanyProfile(
      NAME: $NAME
      LOCATION: $LOCATION
      DESCRIPTION: $DESCRIPTION
      PHONE: $PHONE
      cid: $cid
    ) {
      status
      message
    }
  }
`;

export const addCompanyJobMutation = gql`
  mutation addCompanyJob(
    $cid: String
    $C_NAME: String
    $JOB_TITLE: String
    $APP_DEADLINE: String
    $CITY: String
    $STATE: String
    $COUNTRY: String
    $SALARY: String
    $JOB_DESC: String
    $JOB_CATEGORY: String
  ) {
    addCompanyJob(
      cid: $cid
      C_NAME: $C_NAME
      JOB_TITLE: $JOB_TITLE
      APP_DEADLINE: $APP_DEADLINE
      CITY: $CITY
      STATE: $STATE
      COUNTRY: $COUNTRY
      SALARY: $SALARY
      JOB_DESC: $JOB_DESC
      JOB_CATEGORY: $JOB_CATEGORY
    ) {
      status
      message
    }
  }
`;
